import gpcs.hd.support.action.BaseAction;
import gpcs.hd.support.action.JsonUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class test2 extends BaseAction {

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        
        // 更新车辆
        String u = "http://localhost:9090/PrecisionWork" + "/action/car";
        u += "?handler=WxUpdateCarEndDate";
        u += "&deviceIdList=" + "1000041d-0.02~";
        URL url = new URL(u);
        HttpURLConnection connection = (HttpURLConnection) url
                .openConnection();
        connection.setRequestProperty("x-requested-with",
                "XMLHttpRequest");
        connection.setRequestProperty("Content-Type",
                "application/json");
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(connection.getInputStream(),
                        "UTF-8"));
        String line = null;
        if ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        System.out.println("***********************************");
        
        String d= "123456";
        
        String info = "{\"status\":\"success\",\"send_id\":\"f104221cadc1d9056486f15d53169725\",\"fee\":1,\"sms_credits\":\"44\",\"transactional_sms_credits\":\"0\"}";
        String info2 = "{\"status\":\"error\",\"send_id\":\"f104221cadc1d9056486f15d53169725\",\"fee\":1,\"sms_credits\":\"44\",\"transactional_sms_credits\":\"0\"}";
        if(info2.startsWith("{\"status\":\"success\"")) {
            System.out.println("成功");
        }else {
            System.out.println("失败");
        }

        System.out.println("00AbCdE".toLowerCase());
        System.out.println(filterEmoji("😀 😃 😄 😁 😆 😅 😂 🤣 ☺"));

        // String s = "AKA" + "\n" + "LU"+"\n"+"BENWEI";
        // System.out.println("转换前："+s);
        // s = s.replaceAll("\r|\n", "");
        // System.out.println("转换后："+s);

        String s = "\"ABC\":\"12\n3\"";
        System.out.println("转换前：" + s);
        s = s.replaceAll("\r|\n", "");
        System.out.println("转换后：" + s);

        // 670f68e3a8046fbfdc3c9df5bb562278
        // 670f68e3a8046fbfdc3c9df5bb562278
        System.out.println(MD5("huida110236"));

        int rand = (int) (1 + Math.random() * (999999999 - 1 + 1));
        System.out.println(rand);
        System.out.println(new Date());
        Map map = new HashMap();
        map.put("Guid", "01");
        map.put("InterfaceType", new Integer(1));
        map.put("terminalCode", "666");
        map.put("terminalImsi", "999");
        map.put("NStatus", new Integer(1));
        map.put("NDesc", "test");

        JSONArray jsonMembers = new JSONArray();
        JSONObject member1 = new JSONObject();
        member1.put("loginname", "zhangfan");
        member1.put("password", "userpass");
        member1.put("email", "10371443@qq.com");
        member1.put("sign_date", "2007-06-12");
        JSONObject member2 = new JSONObject();
        member2.put("loginname2", "zhangfan2");
        member2.put("password", "userpass2");
        member2.put("email", "10371443@qq.com2");
        member2.put("sign_date", "2007-06-122");
        jsonMembers.add(member1);
        jsonMembers.add(member2);

        System.out.println(JsonUtils.formatJson(jsonMembers));
        String[] ak = "1,2,3".split(",");
        System.out.println(useList(ak, "6"));

        Employee employee = new Employee();
        employee.setName("wjl");
        employee.setSex("female");
        employee.setAge("24");

        String sql = "123456789,";
        System.out.println(sql.substring(0, sql.length() - 1));

        SimpleDateFormat timeFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        String installDate = timeFormat.format(new Date());
        String endDate = (Integer.parseInt(installDate.substring(0, 4)) + 1)
                + installDate.substring(4, 10);

        System.out.println(installDate + " " + endDate);
        System.out.println(installDate.substring(0, 16));

    }

    private static boolean useList(String[] arr, String targetValue) {
        return Arrays.asList(arr).contains(targetValue);
    }

    public static String MD5(String key) throws Exception {
        char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f' };
        try {
            byte[] btInput = key.getBytes();
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 检测是否有emoji字符
     * 
     * @param source
     * @return 一旦含有就抛出
     */
    public static boolean containsEmoji(String source) {
        if (StringUtils.isBlank(source)) {
            return false;
        }
        int len = source.length();
        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);
            if (!isNotEmojiCharacter(codePoint)) {
                // 判断到了这里表明，确认有表情字符
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否为非Emoji字符
     * 
     * @param codePoint
     *            比较的单个字符
     * @return
     */
    private static boolean isNotEmojiCharacter(char codePoint) {
        return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA)
                || (codePoint == 0xD)
                || ((codePoint >= 0x20) && (codePoint <= 0xD7FF))
                || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD))
                || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
    }

    /**
     * 过滤emoji 或者 其他非文字类型的字符
     * 
     * @param source
     * @return
     */
    public static String filterEmoji(String source) {
        if (StringUtils.isBlank(source)) {
            return source;
        }
        if (!containsEmoji(source)) {
            return source;// 如果不包含，直接返回
        }
        StringBuilder buf = new StringBuilder();
        int len = source.length();
        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);
            if (isNotEmojiCharacter(codePoint)) {
                buf.append(codePoint);
            }
        }

        return buf.toString().trim();
    }

    public static String getSql() {
        String code = "0102030405";
        int ci = code.length() / 2;
        String sql = "";

        for (int i = 0; i < ci; i++) {
            sql += " SELECT CASE WHEN COUNT(a.districtCode)=0 THEN ( ";
        }

        for (int i = 1; i < ci + 1; i++) {
            if (i == 1) {
                sql += " '' ";
            }
            sql += ") ELSE a.districtCode END  ";
            sql += "FROM tb_dept a INNER JOIN   tb_districts b  ON a.districtCode=b.`code` WHERE  1=1 ";
            sql += "AND a.regionCode LIKE '" + code.substring(0, (i * 2))
                    + "%'";

        }

        return sql;
    }

}
