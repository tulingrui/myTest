import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import jxl.Sheet;
import jxl.Workbook;

public class haha {

    /**
     * 
     * @author mzm
     */
    public static void main(String[] args) {
        Long start = System.currentTimeMillis();
        String no = getFileName();
        if (no != null && !"".equals(no)) {
            String[] strs = no.split(",");
            int wenjianToal = 0;// 执行文件总数
            int zhixingNo = 0;// 已经执行数
            for (int i = 0; i < strs.length; i++) {
                wenjianToal++;
                int total = strs.length;
                String gpcsNo = strs[i];// 遍历设备号
                try {
                    /*
                     * 需要修改参数的地方
                     */
                    // 创建Workbook对象
                    Workbook book = Workbook.getWorkbook(new File("d:\\"
                            + gpcsNo + ".xls"));// 修改路径
                    // 得到所有sheet集合
                    Sheet[] sheets = book.getSheets();
                    int a = 0;
                    File file = new File("d://" + gpcsNo + ".txt");// 生成txt的sql文件
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    FileWriter fw = new FileWriter(file.getAbsoluteFile());
                    BufferedWriter bw = new BufferedWriter(fw);

                    zhixingNo++;
                    for (int j = 0; j < sheets.length; j++) {
                        String str="[";
                        for (int k = 0; k < sheets[j].getRows(); k++) {
                            a++;
                            String coopName1 = 
                                     sheets[j].getCell(0, k).getContents();// 时间
                            String coopName2 = 
                                     sheets[j].getCell(1, k).getContents();// 时间
                            String coopName3 = 
                                     sheets[j].getCell(2, k).getContents();// 时间
                            String coopName4 = 
                                     sheets[j].getCell(3, k).getContents();// 时间
                            String coopName5 = 
                                     sheets[j].getCell(4, k).getContents();// 时间
                            String coopName6 = 
                                     sheets[j].getCell(5, k).getContents();// 时间
                            // 将信息添加到list中
                        
                        str+="{";
                        str+="\"zhengshuhao\":\""+coopName1+"\",";
                        str+="\"mingcheng\":\""+coopName2+"\",";
                        str+="\"dizhi\":\""+coopName3+"\",";
                        str+="\"faren\":\""+coopName4+"\",";
                        str+="\"dengji\":\""+coopName5+"\",";
                        str+="\"fanwei\":\""+coopName6+"\"";
                        str+="},";       
                        }
                        str+="]";
                        bw.write(str);
                        bw.newLine();
                    }

                    bw.close();
                    System.out.println("设备号:" + gpcsNo + ",共计" + a + "个点");
                    // 关闭book对象
                    book.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println("*********已经执行第" + zhixingNo + "个文件,共"
                        + total + "个文件***********");
                // System.out.println(Float.parseFloat(zhixingNo+"")/Float.parseFloat(total+""));
                float f = Float.parseFloat(zhixingNo + "")
                        / Float.parseFloat(total + "");
                int count = Math.round(f * 100);
                System.out.println("执行率" + count + "%");
            }
            System.out.println("**************共执行" + wenjianToal
                    + "个文件****************");
        }

        // 要执行的程序的时间
        Long end = System.currentTimeMillis();
        long time = end - start;

        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("ss");
        String dateStr = sdf.format(date);
        System.out.println("执行时间" + dateStr + "秒");
    }

    // 读取文件路径
    public static String getFileName() {
        String path = "D:/"; // 路径
        File f = new File(path);
        if (!f.exists()) {
            System.out.println(path + " not exists");
            return null;
        }

        File fa[] = f.listFiles();
        String str = "";
        for (int i = 0; i < fa.length; i++) {
            File fs = fa[i];
            if (fs.isDirectory()) {
                // System.out.println(fs.getName() + " [目录]");
            } else {
                if (fs.getName().contains(".xls")) {// 判断是否为excel文件
                    String no = fs.getName().substring(0, 5);// 获得设备号
                    str = str + no + ",";
                }
            }
        }
        return str;
    }

    static void changeXls(String path) {

        System.out.println("路径***" + path);

    }
}