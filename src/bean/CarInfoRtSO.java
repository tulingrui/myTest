package bean;

import gpcs.hd.support.service.BaseSO;

@SuppressWarnings("serial")
/**
 * (tb_carinfo_rt)
 * 
 * @version 1.0.0 2017-01-09
 */
public class CarInfoRtSO extends BaseSO {

    /**  */
    private String deviceId;

    /**  */
    private String updateTime;

    /**  */
    private String gpsTime;

    /**  */
    private int latitude;

    /**  */
    private int longitude;

    /**  */
    private int speed;

    /**  */
    private int angle;

    /**  */
    private String identifyId;
    
    /**  */
    private int depth;

    /**  */
    private String cameraType;

    /**  */
    private String depthInterval;
    
    private String[] test;

    /**
     * ��ȡ
     * 
     * @return
     */
    public String getDeviceId() {
        return this.deviceId;
    }

    /**
     * ���
     * 
     * @param deviceId
     * 
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * ��ȡ
     * 
     * @return
     */
    public String getUpdateTime() {
        return this.updateTime;
    }

    /**
     * ���
     * 
     * @param updateTime
     * 
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * ��ȡ
     * 
     * @return
     */
    public String getGpsTime() {
        return this.gpsTime;
    }

    /**
     * ���
     * 
     * @param gpsTime
     * 
     */
    public void setGpsTime(String gpsTime) {
        this.gpsTime = gpsTime;
    }

    /**
     * ��ȡ
     * 
     * @return
     */
    public int getLatitude() {
        return this.latitude;
    }

    /**
     * ���
     * 
     * @param latitude
     * 
     */
    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    /**
     * ��ȡ
     * 
     * @return
     */
    public int getLongitude() {
        return this.longitude;
    }

    /**
     * ���
     * 
     * @param longitude
     * 
     */
    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    /**
     * ��ȡ
     * 
     * @return
     */
    public int getSpeed() {
        return this.speed;
    }

    /**
     * ���
     * 
     * @param speed
     * 
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * ��ȡ
     * 
     * @return
     */
    public int getAngle() {
        return this.angle;
    }

    /**
     * ���
     * 
     * @param angle
     * 
     */
    public void setAngle(int angle) {
        this.angle = angle;
    }

    /**
     * ��ȡ
     * 
     * @return
     */
    public int getDepth() {
        return this.depth;
    }

    /**
     * ���
     * 
     * @param depth
     * 
     */
    public void setDepth(int depth) {
        this.depth = depth;
    }

    /**
     * ��ȡ
     * 
     * @return
     */
    public String getIdentifyId() {
        return this.identifyId;
    }

    /**
     * ���
     * 
     * @param identifyId
     * 
     */
    public void setIdentifyId(String identifyId) {
        this.identifyId = identifyId;
    }

    public String getCameraType() {
        return cameraType;
    }

    public String getDepthInterval() {
        return depthInterval;
    }

    public void setCameraType(String cameraType) {
        this.cameraType = cameraType;
    }

    public void setDepthInterval(String depthInterval) {
        this.depthInterval = depthInterval;
    }

    public String[] getTest() {
        return test;
    }

    public void setTest(String[] test) {
        this.test = test;
    }

}