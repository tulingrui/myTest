import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class sbTEST {

    /**
     * @param args
     */
    public static void main(String[] args) {
        String str2 = "01050206";
        String reg2 = "";
        String res = "";
        for(int i = 0; i < str2.length() / 2; i++)
        {
            reg2 += "(\\d{2})";
            res += "$" + (i + 1);
            if(i != str2.length() / 2 - 1)
            {
                res += "~";
            }
        }
        String [] sb = str2.replaceAll(reg2, res).split("~");
        List<String> d =  new ArrayList<String>();
        String d2 =  "";
        for(int i=0;i<sb.length-1;i++)
        {
            if(i>0) {
                d.add(d.get(i-1)+sb[i]+"");
            }else {
                d.add(""+sb[i]);
            }
           if(i==sb.length-2) {
               d2+="'p"+d.get(i)+"'";
           }else
           {
               d2+="'p"+d.get(i)+"',";
           }
        }
        System.out.println(d.toString());
        System.out.println(d2.toString());
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String ymd = sdf.format(new Date());
        System.out.println(ymd.substring(0,8));
        
        Object [] aka = new Object[] {"A","B","C","D"};
        System.out.println(aka[2].toString());
        
        List<String> ab = new ArrayList<String>();
        ab.add("1");
        ab.add("2");
        ab.add("3");
        ab.set(2,"6666666");
        
        System.out.println(ab.get(2));
        
        System.out.println("2015-10-05".substring(2, 4));
        
    }

}
