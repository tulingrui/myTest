import com.csvreader.CsvReader;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

@SuppressWarnings("serial")
public class writeExcel extends JFrame {
    static JTextField TextField;

    static writeExcel testFrame;

    /**
     * @param args
     * @throws IOException
     */
    public static String shuru = "";

    public static String shuchu = "";

    public static JButton button = new JButton("选择文件路径");

    public static JButton button2 = new JButton("选择输出文件位置");

    public static JButton button3 = new JButton("确定转换");

    public static JButton button4 = new JButton("关闭");

    public static void main(String[] args) throws Exception {
        testFrame = new writeExcel();
        TextField = new JTextField("");

        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser("./");
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "txt文件", "txt");
                chooser.setFileFilter(filter);
                int result = chooser.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    shuru = chooser.getSelectedFile().getAbsolutePath() + "";
                    System.out.println("输入文件路径为:" + shuru);
                    button.setBackground(Color.GRAY);
                }
            }
        });

        button2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser("./");
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int result = chooser.showOpenDialog(null);
                // String fname = chooser.getName(chooser.getSelectedFile());
                if (result == JFileChooser.APPROVE_OPTION) {
                    shuchu = chooser.getSelectedFile().getPath();
                    System.out.println("输出文件路径为：" + shuchu);
                    button2.setBackground(Color.GRAY);
                }
                // Object[] options = { "OK", "CANCEL" };
                // int results = JOptionPane.showOptionDialog(null,
                // "是否选择此位置保存?",
                // "Warning", JOptionPane.DEFAULT_OPTION,
                // JOptionPane.WARNING_MESSAGE, null, options, options[0]);
                // if (results == JOptionPane.OK_OPTION) {
                //
                // }
            }
        });

        button3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Object[] options = { "OK", "CANCEL" };
                if (shuru.equals("")) {
                    JOptionPane.showMessageDialog(null, "输入文件路径未选择！", "提示",
                            JOptionPane.ERROR_MESSAGE);
                } else if (shuchu.equals("")) {
                    JOptionPane.showMessageDialog(null, "输出文件路径未选择！", "提示",
                            JOptionPane.ERROR_MESSAGE);
                } else {
                    int results = JOptionPane.showOptionDialog(null, "是否执行?",
                            "Warning", JOptionPane.DEFAULT_OPTION,
                            JOptionPane.WARNING_MESSAGE, null, options,
                            options[0]);
                    if (results == JOptionPane.OK_OPTION) {
                        try {
                            run(shuru, shuchu);
                            JOptionPane.showMessageDialog(null, "文件已输出成功！",
                                    "提示", JOptionPane.INFORMATION_MESSAGE);
                            shuru = "";
                            shuchu = "";
                            button.setBackground(Color.GREEN);
                            button2.setBackground(Color.GREEN);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        });

        button4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                testFrame.dispose();
            }
        });

        Container contentPane = testFrame.getContentPane();
        testFrame.setTitle("转换");
        button.setBackground(Color.GREEN);
        button2.setBackground(Color.GREEN);
        button3.setBackground(Color.GREEN);
        button4.setBackground(Color.RED);
        contentPane.setLayout(new FlowLayout());
        contentPane.add(button);
        contentPane.add(button2);
        contentPane.add(button3);
        contentPane.add(button4);
        testFrame.setSize(400, 150);
        testFrame.setVisible(true);
    }

    private static void run(String shuru, String shuchu) throws Exception {
        String[] xx = shuru.split("\\\\");
        String[] xx2 = shuchu.split("\\\\");
        String a = "";// 输入路径
        String b = "";// 输出路径
        for (int m = 0; m < xx.length; m++) {
            if (m == 0) {
                a += xx[m] + "//";
            } else if (m == xx.length - 1) {
                a += xx[m];
            } else {
                a += xx[m] + "/";
            }
        }
        for (int m = 0; m < xx2.length; m++) {
            if (m == 0) {
                b += xx2[m] + "//";
            } else {
                b += xx2[m] + "/";
            }
        }
        try {
            OutputStream out = new FileOutputStream(new File(b + "转换后.xls"));
            WritableWorkbook workbook = Workbook.createWorkbook(out);
            WritableSheet sheet = workbook.createSheet("test", 0);
            jxl.write.WritableFont wfontd = null;
            wfontd = new jxl.write.WritableFont(
                    WritableFont.createFont("微软雅黑"), 10);
            WritableCellFormat fontd = new WritableCellFormat(wfontd);
            fontd.setAlignment(Alignment.LEFT);
            fontd.setVerticalAlignment(VerticalAlignment.CENTRE);
            fontd.setBorder(Border.ALL, BorderLineStyle.THIN);
            fontd.setWrap(true);

            int row = 1;
            System.out.println("正在执行 ···");
            File file = new File(a);
            if (file.isFile() && file.exists()) {
                InputStreamReader read = new InputStreamReader(
                        new FileInputStream(file), "GBK");
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTXT = null;
                sheet.addCell(new jxl.write.Label(
                        0,
                        0,
                        "create table #t (deviceId varchar(32) ,workDate date);",
                        fontd));
                String sb = "";
                while ((lineTXT = bufferedReader.readLine()) != null) {
                    String t = lineTXT.toString().trim();
                    sb += t + "; ";
                    if (row == 1) {
                        t = "insert into #t values(" + t.split("deviceId=")[1];
                    } else {
                        t = ")insert into #t values(" + t.split("deviceId=")[1];
                    }

                    t = t.replaceAll("and workDate=", ",");
                    sheet.addCell(new jxl.write.Label(0, row, t, fontd));
                    row++;
                }
                sheet.addCell(new jxl.write.Label(
                        0,
                        row + 1,
                        ")  insert into tb_workStatus(deviceId,workDate,platformId,msg,gisFlag,mtlbFlag,handleTime) select t.deviceId,t.workDate,1,'',0,0,null from #t t left join tb_workStatus w on t.deviceId =w.deviceId and t.workDate=w.workDate where w.deviceId is null;",
                        fontd));
                sheet.addCell(new jxl.write.Label(0, row + 2, "drop table #t;",
                        fontd));
                sheet.addCell(new jxl.write.Label(0, row + 3, sb, fontd));
                System.out.println(row-1+"行");
                read.close();
            } else {
                System.out.println("找不到指定的文件！");
            }
            workbook.write();
            workbook.close(); // 一定要关闭, 否则没有保存Excel
            System.out.println("文件已输出");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}
