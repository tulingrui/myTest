import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class draw {
    public static void main(String[] args) throws IOException {
        InputStream in = new FileInputStream("F://test.png");
        BufferedImage image = new BufferedImage(100,100,BufferedImage.TYPE_INT_RGB);
        image.getGraphics().drawImage(ImageIO.read(in), 0, 0, 100, 100,null);
        image = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY),null).filter(image, null);
        int height = image.getHeight();
        int width = image.getWidth();
        
        for (int y=0;y<height;y++)
        {
            for(int x=0;x<width;x++)
            {
                int rgb = image.getRGB(x, y);
                if(rgb<-300000&&rgb>-800000)
                {
                    System.out.print("..");
                }else if(rgb<-2500000 && rgb>-14342875)
                {
                    System.out.print("--");
                }
                else if(rgb<-14342875)
                {
                    System.out.print("**");
                }else
                {
                    System.out.print("    ");
                }
            }
            System.out.println();
        }
        in.close();
    }

}
