import gpcs.hd.support.action.BaseAction;
import gpcs.hd.support.action.JsonUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class terminal extends BaseAction{

    public static void main(String[] args) throws Exception {
            String u = "http://zwcs.njztc.com:9761/terminal";
            URL url = new URL(u);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            // 是否输入参数
            connection.setDoOutput(true);
            // Post 请求不能使用缓存
            connection.setUseCaches(false);
            // 进行编码   
            connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Charset", "utf-8");
            JSONArray list = new JSONArray();  
            JSONObject obj = new JSONObject();  
            obj.put("terminalCode", "hagongda");  
            obj.put("terminalPassword", "1");  
            obj.put("infoGuid","1");  
            obj.put("cooperCode","1");  
            obj.put("cooperName","1");  
            obj.put("cooperContacts", "1");  
            obj.put("cooperContactsTel", "1");  
            obj.put("cooperProvince", "1");  
            obj.put("cooperCity", "1");  
            obj.put("cooperCounty","1");  
            obj.put("farmerMachineLicense","1");  
            obj.put("farmerMachineBrand", "1");  
            obj.put("farmerMachineHeading", "1");  
            obj.put("farmerMachineModel", "1");  
            obj.put("terminalBrand", "1");  
            obj.put("terminalModel","1");  
            obj.put("terminalImsi","1");  
            obj.put("terminalToolNumber", 1);  
            
            list.add(obj);
            String obj2 = JsonUtils.formatJson(obj);
//            System.out.println("加密后："+encrypt(obj2));
//            System.out.println("加密前："+decrypt(encrypt(obj2)));
            
            String content = "json="+encrypt(obj2);
            connection.getOutputStream().write(content.getBytes());
            connection.connect();
            connection.getOutputStream().flush();
            connection.getOutputStream().close();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                System.out.println(new String(decrypt(line).getBytes(),"UTF-8"));
            }
            reader.close();
            connection.disconnect();
        }
    
    public static byte[] desCrypto(byte[] datasource, String password) {  //DES加密            
        try{  
        SecureRandom random = new SecureRandom();  
        DESKeySpec desKey = new DESKeySpec(password.getBytes());  
        //创建一个密匙工厂，然后用它把DESKeySpec转换成  
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");  
        SecretKey securekey = keyFactory.generateSecret(desKey);  
        //Cipher对象实际完成加密操作  
        Cipher cipher = Cipher.getInstance("DES");  
        //用密匙初始化Cipher对象  
        cipher.init(Cipher.ENCRYPT_MODE, securekey, random);  
        //现在，获取数据并加密  
        //正式执行加密操作  
        return cipher.doFinal(datasource);  
        }catch(Throwable e){  
                e.printStackTrace();  
        }  
        return null;  
    }  
    
    private static byte[] deCrypt(byte[] src, String password) throws Exception {  //DES解密
        // DES算法要求有一个可信任的随机数源  
        SecureRandom random = new SecureRandom();  
        // 创建一个DESKeySpec对象  
        DESKeySpec desKey = new DESKeySpec(password.getBytes());  
        // 创建一个密匙工厂  
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");  
        // 将DESKeySpec对象转换成SecretKey对象  
        SecretKey securekey = keyFactory.generateSecret(desKey);  
        // Cipher对象实际完成解密操作  
        Cipher cipher = Cipher.getInstance("DES");  
        // 用密匙初始化Cipher对象  
        cipher.init(Cipher.DECRYPT_MODE, securekey, random);  
        // 真正开始解密操作  
        return cipher.doFinal(src);  
}  
    
    
    public final static  String decrypt(String ciphertext)   // 解密
    {
      try
      {
        return new String(deCrypt(hex2byte(ciphertext.getBytes()), 
          "anjoulee*&$#@!%^"));
      } catch (Exception localException) {
      }
      return null;
    }
    public final static  String encrypt(String ciphertext) //加密
    {
      try
      {
        return byte2hex(desCrypto(ciphertext.getBytes(), 
          "anjoulee*&$#@!%^"));
      } catch (Exception localException) {
      }
      return null;
    }

    private static final String byte2hex(byte[] b)
    {
      String hs = "";
      String stmp = "";
      for (int n = 0; n < b.length; n++) {
        stmp = Integer.toHexString(b[n] & 0xFF);
        if (stmp.length() == 1)
          hs = hs + "0" + stmp;
        else
          hs = hs + stmp;
      }
      return hs.toUpperCase();
    }

    private static final byte[] hex2byte(byte[] b) {
      if (b.length % 2 != 0)
        throw new IllegalArgumentException("长度不是偶数");
      byte[] b2 = new byte[b.length / 2];
      for (int n = 0; n < b.length; n += 2) {
        String item = new String(b, n, 2);
        b2[(n / 2)] = ((byte)Integer.parseInt(item, 16));
      }
      return b2;
    }
}
