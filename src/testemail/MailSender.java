package testemail;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class MailSender {
    /**
     * @param args
     */
    static int sb = 0;
    static int count = 0;
    public static void main(String[] args) {
        for (int i = 0; i < 200; i++) {
            CountingThread thread = new CountingThread();
            thread.start();
            System.out.println("子线程"+i+"");
            if (i == 199) {
                System.out.println("----------开始----------");
                sb = 1;
            }
        }
    }
}

class CountingThread extends Thread {
    public void run() {
        // System.out.println();
        // System.out.println("子线程 " + this + " 开始"); //
        // this显示本对象信息，[Thread-0,5,main]依次是线程名，优先级，该线程的父线程名字
        // System.out.println();
        while (true) {
            if (MailSender.sb == 1) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
                System.out.println("子线程 " + this + ";"+df.format(new Date())+" 开始");
                Email email = new SimpleEmail();
                // 邮件服务器
                // email.setHostName("smtp.qq.com");//QQ邮箱
                email.setHostName("smtp.exmail.qq.com");// 企业邮箱
                // 端口号
                email.setSmtpPort(465);
                // 用户名、密码
                email.setAuthenticator(new DefaultAuthenticator(
                        "oa@huidatech.cn", "Tulingrui123."));
                email.setSSLOnConnect(true);
                try {
                    // 发件人地址
                    email.setFrom("oa@huidatech.cn");
                    // 邮件标题
                    email.setSubject("TestMail" + this);
                    // 邮件正文
                    email.setMsg("createTime :"+df.format(new Date())+"This is a test mail ... :-)");
                    // 收件人地址
                    //email.addTo("wang.xin@huidatech.cn");
                    email.addTo("2544552541@qq.com");
                    email.send();

                } catch (EmailException e) {
                    System.out.println("---------------------------线程"+this);
                    MailSender.count+=1;
                    System.out.println( MailSender.count);
                    // e.printStackTrace();
                }
                break;
            }
        }
    }
}