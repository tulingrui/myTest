import gpcs.hd.support.action.BaseAction;
import gpcs.hd.support.action.JsonUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class test extends BaseAction {

    public static void main(String[] args) throws Exception {
        
        //http://127.0.0.1:9090/hdPrecisionAgri/action/hlj?handler=getWorkCount&regionCode=0102&dateyear=2017
        String u = "http://47.93.81.41:8092/hdPrecisionAgri/action/hlj";
//        String u = "http://192.168.1.103:9090/hdPrecisionAgri/action/hlj";
        u += "?handler=query13TypeCarList";
        u += "&regionCode=01";
//        u += "&beginDate=2000-01-01";
//        u += "&endDate=2017-12-30";
        u += "&begin=1";
        u += "&end=50";
        
//        System.out.println(u);
        URL url = new URL(u);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("x-requested-with", "XMLHttpRequest");
        connection.setRequestProperty("Content-Type", "application/json");
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                connection.getInputStream(), "UTF-8"));
        String line = null;
        while ((line = reader.readLine()) != null) {
            System.out.println((line));
        }
        reader.close();
        connection.disconnect();
    }

    public static byte[] desCrypto(byte[] datasource, String password) { // DES加密
        try {
            SecureRandom random = new SecureRandom();
            DESKeySpec desKey = new DESKeySpec(password.getBytes());
            // 创建一个密匙工厂，然后用它把DESKeySpec转换成
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey securekey = keyFactory.generateSecret(desKey);
            // Cipher对象实际完成加密操作
            Cipher cipher = Cipher.getInstance("DES");
            // 用密匙初始化Cipher对象
            cipher.init(Cipher.ENCRYPT_MODE, securekey, random);
            // 现在，获取数据并加密
            // 正式执行加密操作
            return cipher.doFinal(datasource);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }

    private static byte[] deCrypt(byte[] src, String password) throws Exception { // DES解密
        // DES算法要求有一个可信任的随机数源
        SecureRandom random = new SecureRandom();
        // 创建一个DESKeySpec对象
        DESKeySpec desKey = new DESKeySpec(password.getBytes());
        // 创建一个密匙工厂
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        // 将DESKeySpec对象转换成SecretKey对象
        SecretKey securekey = keyFactory.generateSecret(desKey);
        // Cipher对象实际完成解密操作
        Cipher cipher = Cipher.getInstance("DES");
        // 用密匙初始化Cipher对象
        cipher.init(Cipher.DECRYPT_MODE, securekey, random);
        // 真正开始解密操作
        return cipher.doFinal(src);
    }

    public final static String jiemi(String ciphertext) // 解密
    {
        try {
            return new String(deCrypt(hex2byte(ciphertext.getBytes()),
                    "anjoulee*&$#@!%^"));
        } catch (Exception localException) {
        }
        return null;
    }

    public final static String jiami(String ciphertext) // 加密
    {
        try {
            return byte2hex(desCrypto(ciphertext.getBytes(), "anjoulee*&$#@!%^"));
        } catch (Exception localException) {
        }
        return null;
    }

    private static final String byte2hex(byte[] b) {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0xFF);
            if (stmp.length() == 1)
                hs = hs + "0" + stmp;
            else
                hs = hs + stmp;
        }
        return hs.toUpperCase();
    }

    private static final byte[] hex2byte(byte[] b) {
        if (b.length % 2 != 0)
            throw new IllegalArgumentException("长度不是偶数");
        byte[] b2 = new byte[b.length / 2];
        for (int n = 0; n < b.length; n += 2) {
            String item = new String(b, n, 2);
            b2[(n / 2)] = ((byte) Integer.parseInt(item, 16));
        }
        return b2;
    }
}
