var context = {
	host : "/product", // web部署
	webFilePath : "",
	loginUser : null,
	getUserCookie : function() {
		var user = null;
		var c = jsYao.getCookie("cookie_json_userLogin");
		if (c != null && c != "\"\"")
			user = eval("(" + decodeURIComponent(c) + ")");
		return user;
	},
	importBase : function() {
		var script = [];
		script.push("<script type=\"text/javascript\" src=\"");
		script.push(this.host);
		script.push("/common/hd/util.js\"></script>");

		script.push("<script type=\"text/javascript\" src=\"");
		script.push(this.host);
		script.push("/common/jquery/jquery-1.9.1.js\"></script>");

		document.write(script.join(''));

	},
	toString16 : function(obj) {
		if (obj.substring(0, 1) != "0") {
			return "0" + (Number(obj).toString(16)).toUpperCase();
		}
		return obj;
	},
	init : function() {
		this.importBase();
	}
};

function Alert(content, title) {
	if (title == null) {
		title = "提示";
	}
	layer.open({
		title : title,
		content : content
	});
}
context.init();