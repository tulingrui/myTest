function initGoogle() {
	var googleLayer1 = new AMap.TileLayer(
			{
				getTileUrl : 'http://mt.google.cn/vt/lyrs=s@157&hl=zh-CN&gl=cn&x=[x]&y=[y]&z=[z]&s=',
			});
	var googleLayer2 = new AMap.TileLayer(
			{
				getTileUrl : 'http://mt.google.cn/vt/lyrs=h@177000000&hl=zh-CN&gl=cn&x=[x]&y=[y]&z=[z]&s=Gali',
			});

	return [ googleLayer1, googleLayer2 ];
}