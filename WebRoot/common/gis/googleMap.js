//document.write("<script type=\"text/javascript\" src=\"https://maps.google.com/maps/api/js?sensor=false\"></script>");
//document.write("<script type=\"text/javascript\" src=\"http://ditu.google.cn/maps/api/js?v=3&sensor=false\"></script>");
//document.write('<script type="text/javascript" src="http://ditu.google.cn/maps/api/js?v=3&key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>');
document
		.write('<script type="text/javascript" src="http://ditu.google.cn/maps/api/js?v=3&key=AIzaSyCgTg9VGSsKYKH3EdDjlkPU81XtPqDOVHQ&sensor=true"></script>');

function HDMap() {
	this.map = null;
	this.controlParam = {
		navigationRigth : ""
	};
	this.createMap = function(div, isNavigat) {
		var attribute = {
			center : new google.maps.LatLng(45.742347, 126.661669),
			zoom : 6,
			minZoom : 2,
			mapTypeControl : false
		};
		if (isNavigat != null) {
			attribute = isNavigat;
		}
		this.map = new google.maps.Map(document.getElementById(div), attribute);
	};
	this.polygonTool = {}, this.resetMap = function() {
		this.map.centerAndZoom(new google.maps.LatLng(45.742347, 126.661669));
		this.map.setZoom(6);
	};
	this.addNavigat = function(controlPostion, x, y) {

	};
	this.addScale = function() {

	};
	this.setCenter = function(lat, lng) {
		this.map.setCenter(new google.maps.LatLng(lat, lng));
	};
	this.setCenterLngLat = function(lnglat) {
		this.map.setCenter(lnglat);
	};
	this.setCenterByMarker = function(m) {
		this.map.setCenter(m.getPosition());
	};
	this.setZoom = function(zoom) {
		this.map.setZoom(zoom);
	};
	this.setMapTypeHYBRID = function() {
		this.map.setMapTypeId(google.maps.MapTypeId.HYBRID);
	};
	this.setMapTypeStreet = function() {
		this.map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
	};
	this.setMapTypeSATELLITE = function() {
		this.map.setMapType(google.maps.MapTypeId.SATELLITE);
	};
	this.panTo = function(lat, lng) {
		this.map.panTo(new google.maps.LatLng(lat, lng));
	};
	this.panToLngLat = function(lngLat) {
		this.map.panTo(lngLat);
	};
	this.createLabelOverLay = function(lat, lng, html, zIndex) {
		var label = new google.maps.Marker({
			map : null,
			position : new google.maps.LatLng(lat, lng)
		});
		if (zIndex)
			label.setZIndex(zIndex);
		return label;
	};
	this.createMarker = function(lat, lng, imgURL, zIndex) {
		var m = new google.maps.Marker({
			map : null,
			position : new google.maps.LatLng(lat, lng)
		});
		if (imgURL)
			m.setIcon(imgURL);
		if (zIndex)
			m.setZIndex(zIndex);
		return m;
	};
	this.setMarkerPosition = function(m, lat, lng) {
		m.setPosition(new google.maps.LatLng(lat, lng));
	};
	this.setMarkerLngLat = function(m, latlng) {
		m.setPosition(latlng);
	};
	this.setMarkerIcon = function(m, imgURL) {
		m.setIcon(imgURL);
	};
	this.showOverLay = function(o) {
		o.setMap(this.map)
	}
	this.hideOverLay = function(o) {
		o.setMap(null);
	};
	this.hideOverLayAll = function() {
		return;
	};
	this.createLine = function(color, weight, opacity) {
		var line = new google.maps.Polyline({
			path : [],
			strokeColor : "red",
			strokeWeight : 6,
			strokeOpacity : 1,
			map : null
		});
		if (color)
			line.setOptions({
				strokeColor : color
			});
		if (weight)
			line.setOptions({
				strokeWeight : weight
			});
		if (opacity)
			line.setOptions({
				strokeOpacity : opacity
			});
		return line;
	};
	this.resetLineStyle = function(line, color, weight, opacity) {
		if (color)
			line.setOptions({
				strokeColor : color
			});
		if (weight)
			line.setOptions({
				strokeWeight : weight
			});
		if (opacity)
			line.setOptions({
				strokeOpacity : opacity
			});
	};
	this.addLinePoint = function(line, lat, lng) {
		line.getPath().push(new google.maps.LatLng(lat, lng));
	};
	this.addLinePointLngLat = function(line, lngLat) {
		line.getPath().push(lngLat);
	};
	this.getLinePoint = function(line) {
		return line.getPath().getArray();
	};
	this.refreshLine = function(line) {

	};
	this.createRect = function(lat1, lng1, lat2, lng2, borderColor, fillColor,
			borderWeight, opacity) {
		var rect = new google.maps.Rectangle({
			map : null,
			bounds : new google.maps.LatLngBounds(new google.maps.LatLng(lat1,
					lng1), new google.maps.LatLng(lat2, lng2))
		});
		if (borderColor)
			rect.setOptions({
				strokeColor : borderColor
			});
		if (fillColor)
			rect.setOptions({
				fillColor : fillColor
			});
		if (borderWeight)
			rect.setOptions({
				strokeWeight : borderWeight
			});
		if (opacity)
			rect.setOptions({
				fillOpacity : opacity
			});
		return rect;
	};
	this.createRectByBound = function(bound, borderColor, fillColor,
			borderWeight, opacity) {
		var rect = new google.maps.Rectangle({
			map : null,
			bounds : bound
		});
		if (borderColor)
			rect.setOptions({
				strokeColor : borderColor
			});
		if (fillColor)
			rect.setOptions({
				fillColor : fillColor
			});
		if (borderWeight)
			rect.setOptions({
				strokeWeight : borderWeight
			});
		if (opacity)
			rect.setOptions({
				fillOpacity : opacity
			});
		return rect;
	};
	this.addOverLayClick = function(o, fun) {
		google.maps.event.addListener(o, "click", fun);
	}
	this.createWin = function() {
		return new google.maps.InfoWindow();
	};
	this.setWinMarker = function(win, m) {
		win.setPosition(m.getPosition());
	};
	this.setWinHTML = function(win, html) {
		win.setContent(html);
	};
	this.setWinPosition = function(win, p) {
		win.setPosition(p);
	}
	this.drawAreaBorder = function(jsonURL, color, weight, opacity, cache, id) {
		var m = this.map;
		$.getJSON(jsonURL, function(data) {
			var arr = [];
			for ( var i = 0; i < data.points.length; i++)
				arr.push(new google.maps.LatLng(data.points[i].Latitude,
						data.points[i].Longitude));
			var line = new google.maps.Polyline({
				path : arr,
				strokeColor : "#FFFF00",
				strokeWeight : 3,
				strokeOpacity : 1,
				map : null
			});
			if (color)
				line.setOptions({
					strokeColor : color
				});
			if (weight)
				line.setOptions({
					strokeWeight : weight
				});
			if (opacity)
				line.setOptions({
					strokeOpacity : opacity
				});
			line.setMap(m);
			if (cache)
				cache[id] = line;
		});
	};
	this.setMarkerAniBOUNCE = function(m) {
		m.setAnimation(google.maps.Animation.BOUNCE);
	};
	this.stopMarkerAnimation = function(m) {
		m.setAnimation(null);
	};
	this.checkLngLatMapBound = function(lat, lng) {
		var sw = map.getBounds().getSouthWest();
		var ne = map.getBounds().getNorthEast();
		return sw.lat() < lat && sw.lng() < lon && ne.lat() > lat
				&& ne.lng() > lon;
	};
	this.checkMarkerMapBound = function(m) {
		return this.checkLngLatMapBound(m.getPosition().getLat(), m
				.getPosition().getLng());
	};
	this.getBoundSW = function(b) {
		var latlng = b.getSouthWest();
		return b.getLat + "_" + b.getLng;
	};
	this.getBoundNE = function(b) {
		var latlng = b.getNorthEast();
		return b.getLat + "_" + b.getLng;
	};
	this.createPolygon = function(points) {
	};
	this.setPolygonStyle = function(p, strokeColor, fillColor, strokeWeight,
			strokeOpacity, fillOpacity) {
	};
	this.getLat = function(p) {
	};
	this.getLng = function(p) {
	};
	// this.drawAreaBorder = function(areaName, color, weight, opacity) {
	// var m = this.map;
	// new BMap.Boundary().get(areaName, function(rs) {
	// for (var i = 0; i < rs.boundaries.length; i++) {
	// var latLngs = rs.boundaries[i].split(";");
	// var points = [];
	// for (var j = 1; j < latLngs.length - 1; j++) {
	// var postion = latLngs[j].indexOf(",");
	// var lng = parseFloat(latLngs[j].substr(0, postion));
	// var lat = parseFloat(latLngs[j].substr(postion + 1));
	// points.push(new google.maps.LatLng(lat, lng));
	// }
	// var line = new google.maps.Polyline({
	// path : points,
	// strokeColor : "#FFFF00",
	// strokeWeight : 3,
	// strokeOpacity : 1,
	// map : null
	// });
	// if (color)
	// line.setOptions({
	// strokeColor : color
	// });
	// if (weight)
	// line.setOptions({
	// strokeWeight : weight
	// });
	// if (opacity)
	// line.setOptions({
	// strokeOpacity : opacity
	// });
	// line.setMap(m);
	// }
	// });
	// };
}