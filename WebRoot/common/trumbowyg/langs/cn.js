/*
 * =========================================================== ru.js Russion
 * translation for Trumbowyg http://alex-d.github.com/Trumbowyg
 * =========================================================== Author : Yuri Lya
 */

$.trumbowyg.langs.cn = {
	viewHTML : "html代码",

	formatting : "格式",
	p : "文本",
	blockquote : "段落",
	code : "代码",
	header : "标题",

	bold : "加粗",
	italic : "斜体",
	strikethrough : "删除线",
	underline : "下划线",

	strong : "粗体",
	em : "a",
	del : "b",

	unorderedList : "项目编号",
	orderedList : "数字编号",

	insertImage : "网络图片",
	insertVideo : "网络视频",
	link : "链接",
	createLink : "创建链接",
	unlink : "移除链接",

	justifyLeft : "左对齐",
	justifyCenter : "居中",
	justifyRight : "右对齐",
	justifyFull : "两端对齐",

	horizontalRule : "水平线",

	fullscreen : "全屏",

	close : "关闭",

	submit : "提交",
	reset : "重置",

	invalidUrl : "校验URL",
	required : "必选",
	description : "描述",
	title : "标题",
	text : "正文",
	align : "对齐",
	
	base64 : "本地图片",
	file : "图片",
	errFileReaderNotSupported : "浏览器不支持"
};