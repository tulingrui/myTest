String.prototype.trim = function() {
	return this.replace(/(^\s*)|(\s*$)/g, "");
};

Array.prototype.contains = function(elem) {
	for ( var i = 0; i < this.length; i++) {
		if (this[i] == elem) {
			return true;
		}
	}
	return false;
};

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S") ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function(fmt) { // author: meizz
	var o = {
		"M+" : this.getMonth() + 1, // 月份
		"d+" : this.getDate(), // 日
		"h+" : this.getHours(), // 小时
		"m+" : this.getMinutes(), // 分
		"s+" : this.getSeconds(), // 秒
		"q+" : Math.floor((this.getMonth() + 3) / 3), // 季度
		"S" : this.getMilliseconds()
	// 毫秒
	};
	if (/(y+)/.test(fmt))
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	for ( var k in o)
		if (new RegExp("(" + k + ")").test(fmt))
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
					: (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
};

/*
 * js工具
 */
var jsYao = {
	objToJson : function(obj) {
		return JSON.stringify(obj);
	},
	gethost : function() {
		return window.location.host;
	},
	checkStringNull : function(str, nullValue) {
		var v = "";
		if (nullValue)
			v = nullValue;
		str = str + "";
		return str == null || str.toUpperCase() == "NULL" ? v : str;
	},
	getCookie : function(key) {
		var cookies = document.cookie.split(";");
		for ( var i = 0; i < cookies.length; i++) {
			if (cookies[i].split("=")[0].trim() == key)
				return cookies[i].split("=")[1];
		}
		return null;
	},
	syncRequestString : function(reqURL, reqData) {
		var json = null;
		$.ajax({
			url : reqURL,
			method : "POST",
			data : reqData,
			async : false,
			success : function(data) {
				json = data;
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				json = '{success : false,info : "请求异常，请检查网络是否正常"}';
			}
		});
		return json;
	},

	syncRequest : function(reqURL, reqData) {
		var str = jsYao.syncRequestString(reqURL, reqData);
		return eval("(" + str + ")");
	},
	lockTableHead : function(tableId) {

		if ($("#jsYao_lockTH_div" + "_" + tableId).length > 0)
			$("#jsYao_lockTH_div" + "_" + tableId).remove();

		var table = $("#" + tableId);
		var tableClone = table.clone();
		tableClone.attr("id", tableId + "_clone");
		var parent = table.parent();
		var div = $("<div></div>");
		div.attr("class",parent.attr("class"));
		div.attr("id", "jsYao_lockTH_div" + "_" + tableId);
		div.append(tableClone);
		div.css("position", "absolute");
		div.css("left", parent.position().left);
		div.css("top", parent.position().top);
		div.width(parent.width());
		div.height(table.find("thead").height());
		var obj = parent[0];
		if (obj.scrollHeight > obj.clientHeight
				|| obj.offsetHeight > obj.clientHeight)
			div.width(parent.width()
					- (obj.scrollWidth == obj.clientWidth ? obj.offsetWidth
							- obj.clientWidth : obj.scrollWidth
							- obj.clientWidth));
		div.css("overflow-y", "hidden");
		div.css("overflow-x", "hidden");
		div.insertAfter(parent);

		div[0].onmousewheel = function() {
			return false;
		}
	},
	getDateDiff : function(startTime, endTime, diffType) {
		// 将xxxx-xx-xx的时间格式，转换为 xxxx/xx/xx的格式
		startTime = startTime.replace(/-/g, "/");
		endTime = endTime.replace(/-/g, "/");
		// 将计算间隔类性字符转换为小写
		diffType = diffType.toLowerCase();
		var sTime = new Date(startTime); // 开始时间
		var eTime = new Date(endTime); // 结束时间
		// 作为除数的数字
		var divNum = 1;
		switch (diffType) {
		case "second":
			divNum = 1000;
			break;
		case "minute":
			divNum = 1000 * 60;
			break;
		case "hour":
			divNum = 1000 * 3600;
			break;
		case "day":
			divNum = 1000 * 3600 * 24;
			break;
		default:
			break;
		}
		return parseInt((eTime.getTime() - sTime.getTime()) / parseInt(divNum));
	},
	getURLParam : function(name) {
		var url = window.location.href;
		url = url.substring(url.indexOf("?") + 1, url.length);
		var params = url.split("&");
		for ( var i = 0; i < params.length; i++)
			if (params[i].split("=")[0] == name)
				return params[i].split("=").length == 2 ? params[i].split("=")[1]
						: null;
		return null;
	},
	toggleMask : function(action, parent, progress, msg) {
		if (action == "show") {
			// 创建对象
			var mask = $("#divMaskYao");
			if (!mask[0])
				mask = $('<div id="divMaskYao" class="mask" align="center"><div style="height: 30px;"><div style="width: 100%; font-family: 微软雅黑; font-size: 15px; font-weight: bolder; padding-top: 5px;"></div></div></div>')
				// 判断父级容器并适应大小
			if (parent) {
				parent.prepend(mask);
				mask.css("position", "absolute");
				mask.css("width", parent.css("width"));
				mask.css("height", parent.css("height"));
			} else {
				$("body").prepend(mask);
				mask.css("position", "absolute");
				mask.css("width", "100%");
				mask.css("height", "100%");
			}
			$("#divMaskYao div:eq(0)").css("margin-top",
					mask.height() / 2 - 20 + "px");
			// 判断是否展示滚动条
			if (progress == undefined || progress == true) {
				$("#divMaskYao div:eq(0)").addClass(
						"progress progress-striped active");
				$("#divMaskYao div:eq(0)").css("width", "70%");
				$("#divMaskYao div:eq(1)").addClass("bar");
			} else {
				$("#divMaskYao div:eq(0)").removeClass(
						"progress progress-striped active");
				$("#divMaskYao div:eq(0)").css("width", "100%");
				$("#divMaskYao div:eq(1)").removeClass("bar");
				$("#divMaskYao div:eq(1)").css("color", "white");
			}
			// 判断消息
			if (msg)
				$("#divMaskYao div:eq(1)").text(msg);
			else
				$("#divMaskYao div:eq(1)").text("载入中。。。");
			$("#divMaskYao").show();
		} else if ($("#divMaskYao")[0])
			$("#divMaskYao").hide();
	},
	execFunMask : function(fun, time) {
		var t = 300;
		if (time)
			t = time;
		jsYao.toggleMask("show");
		setTimeout(function() {
			fun();
			jsYao.toggleMask("hide");
		}, time);
	},
	// pageHref : function(url, invalidate, forward, target) {
	// if (!$("#pageHrefYaoForm")[0])
	// $("body")
	// .append('<form id="pageHrefYaoForm" action="/action/html" method="POST"
	// target=""><input type="hidden" name="url" value=""> <input type="hidden"
	// name="invalidate" value=""> <input type="hidden" name="forward"
	// value=""></form>');
	// $("#pageHrefYaoForm").attr("target", target);
	// $("#pageHrefYaoForm input:eq(0)").attr("value", url);
	// $("#pageHrefYaoForm input:eq(1)").attr("value", invalidate);
	// $("#pageHrefYaoForm input:eq(2)").attr("value", forward);
	// $("#pageHrefYaoForm").submit();
	// },
	getDPI : function() {
		var arrDPI = new Array();
		if (window.screen.deviceXDPI != undefined) {
			arrDPI[0] = window.screen.deviceXDPI;
			arrDPI[1] = window.screen.deviceYDPI;
		} else {
			var tmpNode = $("<div></div>");
			tmpNode
					.attr(
							"style",
							"width:1in;height:1in;position:absolute;left:0px;top:0px;z-index:99;visibility:hidden");
			$("body").append(tmpNode);
			arrDPI[0] = parseInt(tmpNode[0].offsetWidth);
			arrDPI[1] = parseInt(tmpNode[0].offsetHeight);
			tmpNode.remove();
		}
		return arrDPI;
	},
	mapUtil : {
		getDistance : function(lat1, lng1, lat2, lng2) {
			var radLat1 = this.Rad(lat1);
			var radLat2 = this.Rad(lat2);
			var a = radLat1 - radLat2;
			var b = this.Rad(lng1) - this.Rad(lng2);
			var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
					+ Math.cos(radLat1) * Math.cos(radLat2)
					* Math.pow(Math.sin(b / 2), 2)));
			s = s * 6378137.0;// EARTH_RADIUS;
			s = Math.round(s * 10000) / 10000.0;
			return s;
		},
		getDistance2 : function(lat1, lon1, lat2, lon2) {
			if (arguments.length != 4) {
				return 0;
			}
			var a = 6378137, b = 6356752.3142, f = 1 / 298.257223563;
			var L = this.Rad((lon2 - lon1))
			var U1 = Math.atan((1 - f) * Math.tan(this.Rad(lat1)));
			var U2 = Math.atan((1 - f) * Math.tan(this.Rad(lat2)));
			var sinU1 = Math.sin(U1), cosU1 = Math.cos(U1);
			var sinU2 = Math.sin(U2), cosU2 = Math.cos(U2);
			var lambda = L, lambdaP, iterLimit = 100;
			do {
				var sinLambda = Math.sin(lambda), cosLambda = Math.cos(lambda);
				var sinSigma = Math.sqrt((cosU2 * sinLambda)
						* (cosU2 * sinLambda)
						+ (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda)
						* (cosU1 * sinU2 - sinU1 * cosU2 * cosLambda));
				if (sinSigma == 0)
					return 0;
				var cosSigma = sinU1 * sinU2 + cosU1 * cosU2 * cosLambda;
				var sigma = Math.atan2(sinSigma, cosSigma);
				var sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma;
				var cosSqAlpha = 1 - sinAlpha * sinAlpha;
				var cos2SigmaM = cosSigma - 2 * sinU1 * sinU2 / cosSqAlpha;
				if (isNaN(cos2SigmaM))
					cos2SigmaM = 0;
				var C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha));
				lambdaP = lambda;
				lambda = L
						+ (1 - C)
						* f
						* sinAlpha
						* (sigma + C
								* sinSigma
								* (cos2SigmaM + C * cosSigma
										* (-1 + 2 * cos2SigmaM * cos2SigmaM)));
			} while (Math.abs(lambda - lambdaP) > (1e-12) && --iterLimit > 0);
			if (iterLimit == 0) {
				return NaN
			}
			var uSq = cosSqAlpha * (a * a - b * b) / (b * b);
			var A = 1 + uSq / 16384
					* (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
			var B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));
			var deltaSigma = B
					* sinSigma
					* (cos2SigmaM + B
							/ 4
							* (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B
									/ 6
									* cos2SigmaM
									* (-3 + 4 * sinSigma * sinSigma)
									* (-3 + 4 * cos2SigmaM * cos2SigmaM)));
			var s = b * A * (sigma - deltaSigma);
			var fwdAz = Math.atan2(cosU2 * sinLambda, cosU1 * sinU2 - sinU1
					* cosU2 * cosLambda);
			var revAz = Math.atan2(cosU1 * sinLambda, -sinU1 * cosU2 + cosU1
					* sinU2 * cosLambda);
			return s;
		},
		gpsCorrect : {
			pi : 3.14159265358979324,
			a : 6378245.0,
			ee : 0.00669342162296594323,
			transform : function(wgLat, wgLon) {
				if (this.outOfChina(wgLat, wgLon)) {
					return [ wgLat, wgLon ];
				}
				var dLat = this.transformLat(wgLon - 105.0, wgLat - 35.0);
				var dLon = this.transformLon(wgLon - 105.0, wgLat - 35.0);
				var radLat = wgLat / 180.0 * this.pi;
				var magic = Math.sin(radLat);
				magic = 1 - this.ee * magic * magic;
				var sqrtMagic = Math.sqrt(magic);
				dLat = (dLat * 180.0)
						/ ((this.a * (1 - this.ee)) / (magic * sqrtMagic) * this.pi);
				dLon = (dLon * 180.0)
						/ (this.a / sqrtMagic * Math.cos(radLat) * this.pi);
				return [ wgLat + dLat, wgLon + dLon ]
			},
			outOfChina : function(lat, lon) {
				if (lon < 72.004 || lon > 137.8347)
					return true;
				if (lat < 0.8293 || lat > 55.8271)
					return true;
				return false;
			},
			transformLat : function(x, y) {
				var ret = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x
						* y + 0.2 * Math.sqrt(Math.abs(x));
				ret += (20.0 * Math.sin(6.0 * x * this.pi) + 20.0 * Math
						.sin(2.0 * x * this.pi)) * 2.0 / 3.0;
				ret += (20.0 * Math.sin(y * this.pi) + 40.0 * Math.sin(y / 3.0
						* this.pi)) * 2.0 / 3.0;
				ret += (160.0 * Math.sin(y / 12.0 * this.pi) + 320 * Math.sin(y
						* this.pi / 30.0)) * 2.0 / 3.0;
				return ret;
			},
			transformLon : function(x, y) {
				var ret = 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1
						* Math.sqrt(Math.abs(x));
				ret += (20.0 * Math.sin(6.0 * x * this.pi) + 20.0 * Math
						.sin(2.0 * x * this.pi)) * 2.0 / 3.0;
				ret += (20.0 * Math.sin(x * this.pi) + 40.0 * Math.sin(x / 3.0
						* this.pi)) * 2.0 / 3.0;
				ret += (150.0 * Math.sin(x / 12.0 * this.pi) + 300.0 * Math
						.sin(x / 30.0 * this.pi)) * 2.0 / 3.0;
				return ret;
			}
		},
		Rad : function(d) {
			return d * Math.PI / 180.0;
		},
		isPointInPolygon : function(lat, lng, points) {
			var j = 0;
			var s, q;
			s = points[0];
			for ( var f = 1; f <= points.length; ++f) {
				q = points[f % points.length];
				if (lat < Math.min(s.lat, q.lat)
						|| lat > Math.max(s.lat, q.lat)) {
					s = q;
					continue
				}
				if (lat > Math.min(s.lat, q.lat)
						&& lat < Math.max(s.lat, q.lat)) {
					if (lng <= Math.max(s.lng, q.lng)) {
						if (s.lat == q.lat && lng >= Math.min(s.lng, q.lng))
							return true;
						if (s.lng == q.lng) {
							if (s.lng == lng)
								return true;
							else
								++j

						} else {
							var r = (lat - s.lat) * (q.lng - s.lng)
									/ (q.lat - s.lat) + s.lng;
							if (Math.abs(lng - r) < (2e-10))
								return true;

							if (lng < r)
								++j
						}
					}
				} else if (lat == q.lat && lng <= q.lng) {
					var m = points[(f + 1) % points.length];
					if (lat >= Math.min(s.lat, m.lat)
							&& lat <= Math.max(s.lat, m.lat))
						++j
					else
						j += 2
				}
				s = q
			}
			if (j % 2 == 0) {
				return false;
			} else {
				return true;
			}
		},
		gisPointsCache : function() {
			this.pointsFree = [];
			this.pointsUsed = [];
			this.getPoint = function(sys) {
				var point;
				if (this.pointsFree.length > 0) {
					point = this.pointsFree[this.pointsFree.length - 1];
					this.pointsFree.length -= 1;
				} else
					point = this.newPoint(sys);
				this.pointsUsed.push(point);
				return point;
			};
			this.reset = function() {
				if (this.pointsUsed.length == 0)
					return;
				for ( var i = 0; i < this.pointsUsed.length; i++)
					this.pointsFree.push(this.pointsUsed[i]);
				this.pointsUsed.length = 0;
			};
			this.newPoint = function(sys) {
				var s;
				if (sys)
					s = sys;
				else
					s = new esri.symbol.PictureMarkerSymbol("", 16, 16);
				var point = new esri.Graphic(new esri.geometry.Point({
					x : 0,
					y : 0
				}), s, {});
				return point;
			};
		},
		getLocationByBaidu : function(lat, lng, fun) {
			new BMap.Geocoder().getLocation(new BMap.Point(lng, lat), function(
					rs) {
				fun(rs.address);
			});
		}
	},
	//过滤特殊符号~^
	inputCheck:function(s)  
	{ 
		var pattern = new RegExp("[~^]") 
		var rs = ""; 
		var zt = false;
		for (var i = 0; i < s.length; i++) { 
			rs = rs+s.substr(i, 1).replace(pattern, ''); 
			if(s.length==rs.length){
				zt = true;
			}
		} 
		return zt;
	}
};

jsYao.user = eval("("
		+ decodeURIComponent(jsYao.getCookie("cookie_json_userLogin")) + ")");
jsYao.getRootId = function getRootId() {
	return this.user == null ? null : this.user.deptId;
}
jsYao.getRootLevel = function() {
	return this.user == null ? null : this.user.areaLevel;
}
jsYao.getUserType = function() {
	return this.user == null ? null : this.user.userType;
}
jsYao.getRootArea = function() {
	return this.user == null ? null : this.user.deptName;
}
jsYao.getRole = function() {
	return this.user == null ? null : this.user.roleId;
}

jsYao.getDateDiff = function(startTime, endTime, diffType) {
	// 将xxxx-xx-xx的时间格式，转换为 xxxx/xx/xx的格式
	startTime = startTime.replace(/-/g, "/");
	endTime = endTime.replace(/-/g, "/");
	// 将计算间隔类性字符转换为小写
	diffType = diffType.toLowerCase();
	var sTime = new Date(startTime); // 开始时间
	var eTime = new Date(endTime); // 结束时间
	// 作为除数的数字
	var divNum = 1;
	switch (diffType) {
	case "second":
		divNum = 1000;
		break;
	case "minute":
		divNum = 1000 * 60;
		break;
	case "hour":
		divNum = 1000 * 3600;
		break;
	case "day":
		divNum = 1000 * 3600 * 24;
		break;
	default:
		break;
	}
	return parseInt((eTime.getTime() - sTime.getTime()) / parseInt(divNum));
}
//jsYao.lockTableHead = function(tableId) {
//
//	if ($("#jsYao_lockTH_div" + "_" + tableId).length > 0)
//		$("#jsYao_lockTH_div" + "_" + tableId).remove();
//
//	var table = $("#" + tableId);
//	var tableClone = table.clone();
//	tableClone.attr("id", tableId + "_clone");
//	var parent = table.parent();
//	var div = $("<div></div>");
//	div.attr("class",parent.attr("class"));
//	div.attr("id", "jsYao_lockTH_div" + "_" + tableId);
//	div.append(tableClone);
//	div.css("position", "absolute");
//	div.css("left", parent.position().left);
//	div.css("top", parent.position().top);
//	div.width(parent.width());
//	div.height(table.find("thead").height());
//	var obj = parent[0];
//	if (obj.scrollHeight > obj.clientHeight
//			|| obj.offsetHeight > obj.clientHeight)
//		div.width(parent.width()
//				- (obj.scrollWidth == obj.clientWidth ? obj.offsetWidth
//						- obj.clientWidth : obj.scrollWidth
//						- obj.clientWidth));
//	div.css("overflow-y", "hidden");
//	div.css("overflow-x", "hidden");
//	div.insertAfter(parent);
//
//	div[0].onmousewheel = function() {
//		return false;
//	}
//};

jsYao.lockTableNew = function(tableId) {

	if ($("#jsYao_lockTH_div" + "_" + tableId).length > 0)
		$("#jsYao_lockTH_div" + "_" + tableId).remove();

	var table = $("#" + tableId);
	var tableClone = table.clone();
	tableClone.attr("id", tableId + "_clone");
	var parent = table.parent();
	var div = $("<div></div>");
	div.attr("id", "jsYao_lockTH_div" + "_" + tableId);
	div.append(tableClone);
	div.css("position", "absolute");
	div.css("left", parent.position().left);
	div.css("top", parent.position().top);
	div.width(parent.width());
	div.height(table.find("thead").height());
	var obj = parent[0];
	if (obj.scrollHeight > obj.clientHeight
			|| obj.offsetHeight > obj.clientHeight)
		div.width(parent.width()
				- (obj.scrollWidth == obj.clientWidth ? obj.offsetWidth
						- obj.clientWidth : obj.scrollWidth - obj.clientWidth));
	div.css("overflow-y", "hidden");
	div.css("overflow-x", "hidden");
	div.insertAfter(parent);

	div[0].onmousewheel = function() {
		return false;
	}
}

/**
 * 请求等待条
 * 
 * @param {}
 *            parent
 * @param {}
 *            msg
 * @param {}
 *            action
 */
jsYao.toggleMask = function(action, parent, progress, msg) {
	if (action == "show") {
		// 创建对象
		var mask = $("#divMaskYao");
		if (!mask[0])
			mask = $('<div id="divMaskYao" class="mask" align="center"><div style="height: 30px;"><div style="width: 100%; font-family: 微软雅黑; font-size: 15px; font-weight: bolder; padding-top: 5px;"></div></div></div>')
			// 判断父级容器并适应大小
		if (parent) {
			parent.prepend(mask);
			mask.css("position", "absolute");
			mask.css("width", parent.css("width"));
			mask.css("height", parent.css("height"));
		} else {
			$("body").prepend(mask);
			mask.css("position", "absolute");
			mask.css("width", "100%");
			mask.css("height", "100%");
		}
		$("#divMaskYao div:eq(0)").css("margin-top",
				mask.height() / 2 - 20 + "px");
		// 判断是否展示滚动条
		if (progress == undefined || progress == true) {
			$("#divMaskYao div:eq(0)").addClass(
					"progress progress-striped active");
			$("#divMaskYao div:eq(0)").css("width", "70%");
			$("#divMaskYao div:eq(1)").addClass("bar");
		} else {
			$("#divMaskYao div:eq(0)").removeClass(
					"progress progress-striped active");
			$("#divMaskYao div:eq(0)").css("width", "100%");
			$("#divMaskYao div:eq(1)").removeClass("bar");
			$("#divMaskYao div:eq(1)").css("color", "white");
		}
		// 判断消息
		if (msg)
			$("#divMaskYao div:eq(1)").text(msg);
		else
			$("#divMaskYao div:eq(1)").text("载入中。。。");
		$("#divMaskYao").show();
	} else if ($("#divMaskYao")[0])
		$("#divMaskYao").hide();
};
jsYao.execFunMask = function(fun, time) {
	var t = 300;
	if (time)
		t = time;
	jsYao.toggleMask("show");
	setTimeout(function() {
		fun();
		jsYao.toggleMask("hide");
	}, time);
};

/**
 * 页面跳转
 * 
 * @param {}
 *            key
 * @param {}
 *            invalidate
 * @param {}
 *            forward
 * @param {}
 *            target
 */
jsYao.pageHref = function(key, invalidate, forward, target) {
	// 初始化form
	if (!$("#pageHrefYaoForm")[0])
		$("body")
				.append(
						'<form id="pageHrefYaoForm" action="/action/html" method="POST" target=""><input type="hidden" name="url" value=""> <input type="hidden" name="invalidate" value=""> <input type="hidden" name="forward" value=""></form>');
	// 属性赋值
	$("#pageHrefYaoForm").attr("target", target);
	$("#pageHrefYaoForm input:eq(0)").attr("value", key);
	$("#pageHrefYaoForm input:eq(1)").attr("value", invalidate);
	$("#pageHrefYaoForm input:eq(2)").attr("value", forward);
	// 提交
	$("#pageHrefYaoForm").submit();
}

jsYao.getURLParam = function(name) {
	var url = window.location.href;
	url = url.substring(url.indexOf("?") + 1, url.length);
	var params = url.split("&");
	for ( var i = 0; i < params.length; i++)
		if (params[i].split("=")[0] == name)
			return params[i].split("=").length == 2 ? params[i].split("=")[1]
					: null;
	return null;
}

jsYao.checkRole = function(roles) {
	var role = this.getRole();
	if (role == null || !roles.contains(role)) {
		alert("请先进行登录！");
		window.location = "http://" + window.location.host + "?target="
				+ encodeURIComponent(window.location.href);
		return;
	}
}

function yaoSelect(opt) {
	opt = $.extend({
		labelPWidth : 50,
		labelAWidth : 50,
		textWidth : 150,
		menuHeight : 200,
		data : []
	}, opt || {});
	this.dom;
	this.valueSel = null;
	this.init = function() {
		if (!opt.domId)
			return;
		this.dom = $("#" + opt.domId);
		this.dom.css("position", "relative");

		var div = $("<div></div>");
		div.attr("class", "input-append input-prepend");
		if (opt.labelPrepend) {
			var labelP = $("<span></span>")
			labelP.attr("class", "add-on");
			labelP.css("width", opt.labelPWidth + "px");
			labelP.text(opt.labelPrepend);
			div.append(labelP);
		}
		var input = $("<input>");
		input.attr("type", "text");
		input.attr("readOnly", true);
		input.css("width", opt.textWidth + "px");
		div.append(input);
		if (opt.labelAppend) {
			var labelA = $("<span></span>")
			labelA.attr("class", "add-on");
			labelA.css("width", opt.labelAWidth + "px");
			labelA.text(opt.labelAppend);
			div.append(labelA);
		}
		this.dom.append(div);

		var ul = $("<ul></ul>");
		ul.css("position", "absolute");
		ul.css("top", "33px");
		ul.css("width", (opt.textWidth + 8) + "px");
		ul.css("max-height", opt.menuHeight + "px");
		ul.css("overflow-y", "auto");
		ul.css("overflow-x", "hidden");
		ul.css("display", "none");
		ul.css("zIndex", "100000");
		ul.css("border-radius", "3px");
		if (opt.labelPrepend)
			ul.css("left", (opt.labelPWidth + 10) + "px");
		this.dom.append(ul);
		ul.menu();
		input.click(function() {
			ul.show();
		});
		ul.mouseleave(function() {
			ul.hide();
		});
	};
	this.loadDate = function(data) {

		var ul = this.dom.find("ul");
		ul.empty();
		for ( var i = 0; i < data.length; i++)
			ul.append('<li><a href="javascript:void(0)" id="' + opt.domId + '_'
					+ data[i][0] + '_' + data[i][1] + '">' + data[i][1]
					+ '</a></li>');
		ul.menu("refresh");
		var input = this.dom.find("input");
		var that = this;

		ul.find("a").click(function() {
			ul.hide();
			var valueOld = that.valueSel;
			that.valueSel = $(this).attr("id").split("_")[1];
			input.val($(this).attr("id").split("_")[2]);
			if (valueOld != that.valueSel)
				that.valueChange();
		});
	};
	this.valueChange = function() {
	};
	this.getValue = function() {
		return this.valueSel;
	};
	this.reset = function() {
		this.valueSel = null;
		this.dom.find("input").val("");
	};
	this.init();
	this.loadDate(opt.data);
}

function navigationMenuYao(opt) {
	opt = $.extend({
		domId : null,
		menuData : [],
		width : 200,
		minHeight : 200
	}, opt || {});
	this.root = null;
	this.intervalScroll = null;
	this.init = function() {
		this.root = $("#" + opt.domId);
		this.root.addClass("navigationYao");
		this.root.css("width", opt.width + "px");
		this.root.append("<ul class='navYaoULMain'></ul>");
		this.root.append("<ul class='navYaoULItem'></ul>");
	};
	this.callback = function(data) {
		alert(data[0] + "_" + data[1]);
	};
	this.load = function() {
		var ul = this.root.children("ul.navYaoULMain");
		var ul2 = this.root.children("ul.navYaoULItem");
		ul.empty();
		ul2.empty();
		for ( var i = 0; i < opt.menuData.length; i++) {
			ul.append("<li id='navYaoLi_" + i
					+ "' class='navYaoLiMain'><span class='fontWRYH'>"
					+ opt.menuData[i][0] + "</span></li>");
			for ( var j = 0; j < opt.menuData[i][1].length; j++) {
				ul2.append("<li id='navYaoLi_" + i + "_" + j
						+ "' class='navYaoLiItem'><span class='fontWRYH'>"
						+ opt.menuData[i][1][j][0] + "</span></li>");
			}
		}
		var that = this;
		ul.children("li").click(function() {
			var index = $(this).index();
			if (opt.menuData[index][1].length == 0) {
				ul.children("li").removeClass("navYaoLiMainSelected");
				ul2.children("li").removeClass("navYaoLiItemSelected");
				$(this).addClass("navYaoLiMainSelected");
				$(this).removeClass("navYaoLiMainActive");
				that.callback(opt.menuData[index]);
			}
		});
		ul2.children("li").click(function() {
			ul.children("li").removeClass("navYaoLiMainSelected");
			ul2.children("li").removeClass("navYaoLiItemSelected");
			$(this).addClass("navYaoLiItemSelected");
			$(this).removeClass("navYaoLiItemActive");
			var arr = $(this).attr("id").split("_");
			$("#" + arr[0] + "_" + arr[1]).addClass("navYaoLiMainSelected");
			that.callback(opt.menuData[arr[1]][1][arr[2]]);
		});
		ul.mouseleave(function() {
			window.setTimeout(function() {
				if (ul2.hasClass("hover"))
					ul2.hide();
			}, 500);
		});
		ul2.mouseover(function() {
			ul2.addClass("hover");
		});
		ul2.mouseleave(function() {
			ul2.removeClass("hover");
			ul2.hide();
		});
		ul.children("li").mouseover(
				function() {
					if (!$(this).hasClass("navYaoLiMainSelected"))
						$(this).addClass("navYaoLiMainActive");
					var index = $(this).index();
					var children = opt.menuData[index][1];
					if (children.length == 0) {
						ul2.hide();
					} else {
						ul2.find("li").hide();
						for ( var i = 0; i < children.length; i++)
							ul2.find("#navYaoLi_" + index + "_" + i).show();
						var p = $(this).position().top;
						if (p > that.root.height() / 2) {
							// 向上显示
							ul2.css("max-height", that.root.height() + "px");
							ul2.css("top", p > ul2.height() ? (p - ul2.height()
									+ $(this).outerHeight() + 3) : 0 + "px");
						} else {
							// 向下显示
							ul2.css("max-height", (that.root.height() - p)
									+ "px");
							ul2.css("top", (p > 0 ? p : 0) + "px");
						}
						ul2.css("left", (that.root.width() + 3) + "px");
						ul2.show();
					}

				});
		ul.children("li").mouseleave(function() {
			$(this).removeClass("navYaoLiMainActive");
		});
		ul2.children("li").mouseover(function() {
			if (!$(this).hasClass("navYaoLiItemSelected"))
				$(this).addClass("navYaoLiItemActive");
		});
		ul2.children("li").mouseleave(function() {
			$(this).removeClass("navYaoLiItemActive");
		});
	};
	this.init();
	this.load();
}

function linkageMenuYao(opt) {
	opt = $.extend({
		id : null,
		titles : [],
		// [id,parentId,txt,{}]
		data : [],
		ajax : {
			enabled : false,
			url : null
		},
		rootPId : 0,
		buttons : []
	}, opt || {});

	this.container;
	this.confirmValue = [];
	this.interimValue = [];
	this.dataCache = null;

	this.render = function() {
		if (opt.id == null || opt.titles.length == 0)
			return;
		if (opt.ajax.enabled) {
		} else
			this.dataCache = opt.data;
		var con = $("#" + opt.id);
		if (!con)
			return;
		this.container = con;
		// 创建菜单
		var ulHtml = [];
		ulHtml.push('<ul class="nav nav-tabs">');
		var contentHtml = [];
		contentHtml.push('<div class="tab-content">');
		for ( var i = 0; i < opt.titles.length; i++) {
			var id = opt.id + "_menuTab" + i;
			ulHtml.push('<li><a href="#');
			ulHtml.push(id);
			ulHtml.push('" data-toggle="tab">');
			ulHtml.push(opt.titles[i]);
			ulHtml.push('</a></li>');

			contentHtml.push('<div class="tab-pane" id="')
			contentHtml.push(id);
			contentHtml.push('"></div>');
		}
		ulHtml.push('</ul>');
		contentHtml.push('</div>');

		var ul = $(ulHtml.join(""));
		var content = $(contentHtml.join(""));
		con.append(ul);
		con.append(content);

		var buttonDiv = $("<div></div>");
		for ( var i = 0; i < opt.buttons.length; i++) {
			var button = $('<button></button>');
			buttonDiv.append(button);
			if (opt.buttons[i]["css"])
				button.addClass(opt.buttons[i]["css"]);
			if (opt.buttons[i]["style"])
				button.attr("style", opt.buttons[i]["style"]);
			button.text(opt.buttons[i]["txt"]);
			button.click(opt.buttons[i]["fun"]);
		}
		con.append(buttonDiv);

		con.addClass("tabbable");
		con.addClass("linkageMenu");
	};
	this.setDisabled = function(index) {
		var menu = this.container.find("ul a:eq(" + index + ")");
		menu.attr("href", "javascript:void(0)");
		menu.removeAttr("data-toggle");
	};
	this.setTitle = function(index, title) {
		var menu = this.container.find("ul a:eq(" + index + ")");
		if (title) {
			menu.text(title);
			menu.css("font-weight", "bolder");
			menu.css("color", "#CC3333");
		} else {
			menu.text(opt.titles[index]);
			menu.css("font-weight", "normal");
			menu.css("color", "");
		}
	};
	this.setValue = function(index, arr) {
		this.interimValue[index] = arr;
	};
	this.getDataFormCache = function(level, id) {
		var arr = this.dataCache[level];
		for ( var i = 0; i < arr.length; i++)
			if (arr[i][0] == id)
				return arr[i];
	};
	this.loadItemsByPId = function(level, pId) {
		if (!pId && level == 0)
			pId = opt.rootPId;
		var arr = this.dataCache[level];
		var items = [];
		for ( var i = 0; i < arr.length; i++)
			if (level == 0 || arr[i][1] == pId)
				items.push(arr[i]);
		if (items.length > 0 || !opt.ajax.enabled)
			return items;
		var json = jsYao.syncRequest(opt.ajax.url, {
			level : level,
			parentId : pId
		});
		if (json.success) {
			var arr = json.jsonObject[0];
			for ( var i = 0; i < arr.length; i++)
				this.dataCache[level].push(arr[i]);
			return arr;
		} else {
			alert(json.info);
			return [];
		}
	};
	this.loadItem = function(index, parentId) {
		var arr = this.loadItemsByPId(index, parentId);
		var html = [];
		for ( var i = 0; i < arr.length; i++) {
			html.push('<span class="label" id="');
			html.push(opt.id);
			html.push('~menuValue~');
			html.push(index);
			html.push('~');
			html.push(arr[i][0]);
			html.push('">');
			html.push(arr[i][2]);
			html.push('</span>');
		}
		var content = this.container.find(".tab-content div:eq(" + index + ")");
		content.html(html.join(""));
		var that = this;
		content.find("span").click(function() {
			that.itemClick($(this));
		});
		this.container.find("ul a:eq(" + index + ")").click();
	};
	this.itemClickById = function(index, id, confirm) {
		var items = this.container.find(".tab-content div:eq(" + index
				+ ") span");
		for ( var i = 0; i < items.length; i++) {
			if (items.eq(i).attr("id").split("~")[3] == id) {
				this.itemClick(items.eq(i), confirm);
				break;
			}
		}
	};
	this.itemClick = function(item, confirm) {
		var content = item.parent();
		var index = content.index();
		for ( var i = index + 1; i < opt.titles.length; i++) {
			this.setTitle(i);
			this.container.find(".tab-content div:eq(" + i + ")").empty();
			this.setValue(i, null);
		}
		if (item.hasClass("label-info")) {
			item.removeClass("label-info");
			this.setTitle(index);
			this.setValue(index, null);
		} else {
			content.find("span").removeClass("label-info");
			item.addClass("label-info");
			this.setTitle(index, item.text());
			var arr = item.attr("id").split("~");
			var value = this.getDataFormCache(arr[2], arr[3]);
			this.setValue(index, value);
			if (opt.titles[index + 1])
				this.loadItem(index + 1, value[0]);
		}
		if (confirm && confirm == true)
			this.confirm();
	};
	this.confirm = function() {
		this.confirmValue = this.interimValue.concat();
		return this.confirmValue;
	};
	this.recover = function() {
		for ( var i = 0; i < this.interimValue.length; i++) {
			if (this.interimValue[i] == null && this.confirmValue[i] == null)
				break;
			else if (this.interimValue[i] != null
					&& this.confirmValue[i] != null
					&& this.interimValue[i][0] == this.confirmValue[i][0])
				continue;
			if (this.confirmValue[i] == null) {
				this.itemClickById(i, this.interimValue[i][0]);
				break;
			} else if (this.interimValue[i] == null)
				this.itemClickById(i, this.confirmValue[i][0]);
			else
				this.itemClickById(i, this.confirmValue[i][0]);
		}
		this.interimValue = this.confirmValue.concat();

		for ( var i = this.interimValue.length - 1; i >= 0; i--)
			if (this.interimValue[i] != null) {
				if (i + 1 == this.interimValue.length)
					this.container.find("ul a:eq(" + i + ")").click();
				else
					this.container.find("ul a:eq(" + (i + 1) + ")").click();
				break;
			}

	};
	this.render();
	this.loadItem(0);
}

function pageBarYao(opt) {
	opt = $.extend({
		id : null,
		perCount : null,
		pageMaxSize : 11,
		colorSel : "rgba(102,153,255,0.3)",
		queryFun : function(begin, end, pageNo) {
		}
	}, opt || {});

	this.pageInfo = {
		totalCount : 0,
		perCount : 30,

		pageBeginNo : 0,
		pageEndNo : 0,

		pageCount : 0,
		pageNo : 0
	},

	this.container = null;
	this.pageMaxSize = null;

	this.render = function() {
		this.container = $("#" + opt.id);
		this.pageMaxSize = opt.pageMaxSize;
		if (!this.container || this.container.length == 0)
			return;
		if (opt.perCount != null)
			this.pageInfo.perCount = opt.perCount;

		this.container.addClass("pagination");
		this.container.addClass("pagination-centered");

		var ul = $("<ul></ul>");
		this.container.append(ul);

		ul.append('<li><a href="javascript:void(0)">首页</a></li>');
		ul.append('<li><a href="javascript:void(0)">上一页</a></li>');
		for ( var i = 0; i < this.pageMaxSize; i++)
			ul.append($('<li><a href="javascript:void(0)"></a></li>'));
		ul.append('<li><a href="javascript:void(0)">下一页</a></li>');
		ul.append('<li><a href="javascript:void(0)">末页</a></li>');

		var that = this;
		ul.find("li").click(function() {
			that.liClick($(this));
		});
		ul.find("li").hide();
	};
	this.liClick = function(li) {
		if (li.hasClass("disabled"))
			return;
		var index = li.index();
		var targetPN;
		if (index == 0)
			targetPN = 1;
		else if (index == 1)
			targetPN = this.pageInfo.pageNo - 1;
		else if (index == this.pageMaxSize + 2)
			targetPN = this.pageInfo.pageNo + 1;
		else if (index == this.pageMaxSize + 3)
			targetPN = this.pageInfo.pageCount;
		else
			targetPN = index - 2 + this.pageInfo.pageBeginNo;
		opt.queryFun((targetPN - 1) * this.pageInfo.perCount + 1, targetPN
				* this.pageInfo.perCount, targetPN);
	};
	this.changePN = function(pageNo) {
		this
				.changeAll(this.pageInfo.totalCount, pageNo,
						this.pageInfo.perCount);
	};
	this.changeTCPN = function(totalCount, pageNo) {
		this.changeAll(totalCount, pageNo, this.pageInfo.perCount);
	};
	this.changeAll = function(totalCount, pageNo, perCount) {
		this.container.find("li").hide();
		this.container.find("li").removeClass("disabled");
		this.container.find("li a").css("background-color", "");

		var pageCount = Math.ceil(totalCount / perCount);
		if (pageCount == 0)
			return;
		if (pageNo > pageCount)
			pageNo = 0;

		var beginNo;
		var endNo;
		if (pageCount <= this.pageMaxSize) {
			beginNo = 1;
			endNo = pageCount;
		} else {
			if (pageNo <= this.pageMaxSize / 2) {
				beginNo = 1;
				endNo = this.pageMaxSize;
			} else if ((pageCount - pageNo) < (this.pageMaxSize / 2)) {
				endNo = pageCount;
				beginNo = pageCount - this.pageMaxSize + 1;
			} else {
				if (this.pageMaxSize % 2 == 0) {
					beginNo = pageNo - Math.floor(this.pageMaxSize / 2);
					endNo = pageNo + Math.floor(this.pageMaxSize / 2) - 1;
				} else {
					beginNo = pageNo - Math.floor(this.pageMaxSize / 2);
					endNo = pageNo + Math.floor(this.pageMaxSize / 2);
				}
			}
		}

		this.pageInfo.pageBeginNo = beginNo;
		this.pageInfo.pageEndNo = endNo;
		this.pageInfo.pageNo = pageNo;
		this.pageInfo.totalCount = totalCount;
		this.pageInfo.perCount = perCount;
		this.pageInfo.pageCount = pageCount;

		var li;
		for ( var no = beginNo; no <= endNo; no++) {
			li = this.container.find("li:eq(" + ((no - beginNo) + 2) + ")");
			li.find("a").text(no);
			li.show();
		}
		if (pageNo != 0) {
			this.container.find("li:eq(" + ((pageNo - beginNo) + 2) + ")")
					.addClass("disabled");
			this.container.find("li:eq(" + ((pageNo - beginNo) + 2) + ")")
					.find("a").css("background-color", opt.colorSel);
		}

		this.container.find("li:eq(0)").show();
		this.container.find("li:eq(1)").show();
		this.container.find("li:eq(" + (this.pageMaxSize + 2) + ")").show();
		this.container.find("li:eq(" + (this.pageMaxSize + 3) + ")").show();

		if (pageNo <= 1) {
			this.container.find("li:eq(0)").addClass("disabled");
			this.container.find("li:eq(1)").addClass("disabled");
		}
		if (pageNo >= pageCount) {
			this.container.find("li:eq(" + (this.pageMaxSize + 2) + ")")
					.addClass("disabled");
			this.container.find("li:eq(" + (this.pageMaxSize + 3) + ")")
					.addClass("disabled");
		}
	};
	this.render();
}
function selectHD(opt) {
	opt = $.extend({
		id : null,
		data : []

	}, opt || {});

	this.input = null;
	this.container = null;
	this.list = null;
	this.val = null;
	this.getVal = function() {
		return this.val;
	};
	this.reset = function() {
		this.input.val("");
		this.val = null;
	};
	this.setValById = function(id) {
		for ( var i = 0; i < opt.data.length; i++)
			if (opt.data[i][0] == id)
				this.list.find("li:eq(" + i + ")").click();
	};
	this.search = function() {
		this.val = null;
		var a = this.input.val();
		if (a != "") {
			this.list.find("li").hide();
			for ( var i = 0; i < opt.data.length; i++)
				if (opt.data[i][1].indexOf(a) > -1) {
					this.list.find("li:eq(" + i + ")").show();
					if (opt.data[i][1] == a) {
						this.list.find("li:eq(" + i + ")").click();
					}
				}
		} else {
			this.list.find("li").show();
		}
	};
	this.init = function() {
		var that = this;

		if (!opt.id || opt.id == null || $("#" + opt.id).length == 0) {
			return;
		}
		this.input = $("#" + opt.id);
		this.input.keyup(function() {
			that.search();
		});
		this.input.blur(function() {
			if (that.val == null) {
				that.input.val("");
			}

		});
		this.input
				.click(function() {
					that.container.show();
					that.container.css("left", that.input.offset().left + "px");
					if (that.container.height() > ($(document).height() - that.input
							.offset().top)) {
						var bb = that.input.offset().top
								- that.container.height() - 12;
						that.container.css("top", bb + "px");
					} else {
						var aa = that.input.offset().top + that.input.height()
								+ 15;
						that.container.css("top", aa + "px");
					}
					that.container.width(that.input.width() + 5);
					// alert(this.input.position().left);
					that.search();
				});

		this.container = $("<div><div></div></div>");
		$("body").append(this.container);
		this.container.addClass("selectHDOuter");
		var inner = this.container.children("div:eq(0)");
		inner.addClass("selectHD");
		// this.input.after(this.container);
		this.container.hide();
		this.container.mouseleave(function() {
			that.container.hide();
		});

		this.list = $("<ul></ul>");
		inner.append(this.list);
		for ( var i = 0; i < opt.data.length; i++) {
			this.list.append($("<li>" + opt.data[i][1] + "</li>"));
		}
		this.list.find("li").click(function() {
			that.input.val($(this).text());
			that.val = opt.data[$(this).index()][0];
		});

		if (opt.bgColorOuter)
			this.container.css("background-color", opt.bgColorOuter);
		if (opt.bgColorUL)
			this.list.css("background-color", opt.bgColorUL);

	};
	this.init();
}