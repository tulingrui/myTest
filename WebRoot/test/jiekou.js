$(document).ready(function() {
	$("#data").val(formatJson(JSON.stringify(dataDemo)));

	$("#geshihua").click(function() {
		try {
			$("#data").val(formatJson($.trim($("#data").val())));
			$("#data").css("color", "black");
			$("#data").css("background-color", "white");
		} catch (e) {
			$("#data").css("color", "red");
			$("#data").css("background-color", "#e2e2e2");
		}
	});

	$("#access").click(function() {
		var url = "http://";
		url += $("#url").val();
		url += $("#projectName").val();
		var dataJson = $("#data").val();
		var handler = $("#handler").val();
		var data = jQuery.parseJSON(dataJson);
		data["handler"] = handler;
		access(url, data);
	});

});
function access(url, data) {
	$.ajax({
		type : 'POST',
		async : false,
		url : url,
		data : data,
		dataType : "jsonp",
		jsonp : 'jsonpcallback',
		success : function(json) {
			if (!json.success) {
				alert(json.msg);
				return;
			}
			console.log(json, new Date());
			$("#back").append(
					"<br><br>" + new Date() + "<br>" + JSON.stringify(json));
		},
		error : function(e) {
			alert('异常！');
		}
	});
}
var dataDemo = {
	"key" : "",
	"installMan" : "",
	"statusType" : "1",
	"begin" : "1",
	"end" : "8"
};

var formatJson = function(json, options) {
	var reg = null, formatted = '', pad = 0, PADDING = '    '; // one can also
	// use '\t' or a
	// different
	// number of
	// spaces
	// optional settings
	options = options || {};
	// remove newline where '{' or '[' follows ':'
	options.newlineAfterColonIfBeforeBraceOrBracket = (options.newlineAfterColonIfBeforeBraceOrBracket === true) ? true
			: false;
	// use a space after a colon
	options.spaceAfterColon = (options.spaceAfterColon === false) ? false
			: true;

	// begin formatting...

	// make sure we start with the JSON as a string
	if (typeof json !== 'string') {
		json = JSON.stringify(json);
	}
	// parse and stringify in order to remove extra whitespace
	json = JSON.parse(json);
	json = JSON.stringify(json);

	// add newline before and after curly braces
	reg = /([\{\}])/g;
	json = json.replace(reg, '\r\n$1\r\n');

	// add newline before and after square brackets
	reg = /([\[\]])/g;
	json = json.replace(reg, '\r\n$1\r\n');

	// add newline after comma
	reg = /(\,)/g;
	json = json.replace(reg, '$1\r\n');

	// remove multiple newlines
	reg = /(\r\n\r\n)/g;
	json = json.replace(reg, '\r\n');

	// remove newlines before commas
	reg = /\r\n\,/g;
	json = json.replace(reg, ',');

	// optional formatting...
	if (!options.newlineAfterColonIfBeforeBraceOrBracket) {
		reg = /\:\r\n\{/g;
		json = json.replace(reg, ':{');
		reg = /\:\r\n\[/g;
		json = json.replace(reg, ':[');
	}
	if (options.spaceAfterColon) {
		reg = /\:/g;
		json = json.replace(reg, ': ');
	}

	$.each(json.split('\r\n'), function(index, node) {
		var i = 0, indent = 0, padding = '';

		if (node.match(/\{$/) || node.match(/\[$/)) {
			indent = 1;
		} else if (node.match(/\}/) || node.match(/\]/)) {
			if (pad !== 0) {
				pad -= 1;
			}
		} else {
			indent = 0;
		}

		for (i = 0; i < pad; i++) {
			padding += PADDING;
		}

		formatted += padding + node + '\r\n';
		pad += indent;
	});

	return formatted;
};
