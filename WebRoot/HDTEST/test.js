$(document).ready(
		function() {
			var deviceId = jsYao.getURLParam("deviceId");
			var beginTime = decodeURIComponent(jsYao.getURLParam("beginTime"));
			var endTime = decodeURIComponent(jsYao.getURLParam("endTime"));
			if (deviceId == null || deviceId == "" || beginTime == null
					|| beginTime == "" || endTime == null || endTime == "") {
				alert("查询参数不足  请补充参数后刷新页面！");
				return;
			}
			$("#loading").show();
			traceRT.gisInit(deviceId, beginTime, endTime);
		});

var traceRT = {
	gisAttr : {
		map : null,

		lineLayer : null,
		lineWork : null,

		pointCache : null
	},
	computeAttr : {
		pyDisMax : 200,
		pyDisMin : 3,

		schemeDepth : 25,
		depthMax : 100,
		depthMin : 5
	},
	runParam : {
		latLast : 0,
		lngLast : 0,
		depthFlagLast : 0,

		surveyPointsQualified : 0,

		machines : {},
	},
	gisInit : function(deviceId, beginTime, endTime) {
		var that = this;
		if (!esri.Map) {
			window.setTimeout(function() {
				that.gisInit();
			}, 1000);
			return;
		}
		traceRT.gisAttr.map = new esri.Map("mapDiv", {
			slider : false,
			logo : false
		});
		traceRT.gisAttr.map.centerAndZoom(new esri.geometry.Point(100, 40), 1);
		var arr = initGoogle();
		traceRT.gisAttr.map.addLayer(arr[0]);
		traceRT.gisAttr.map.addLayer(arr[1]);
		traceRT.gisAttr.lineLayer = new esri.layers.GraphicsLayer();
		traceRT.gisAttr.map.addLayer(traceRT.gisAttr.lineLayer);

		traceRT.gisAttr.lineRoad = new esri.Graphic(new esri.geometry.Polyline(
				{
					"paths" : [ [ [] ] ]
				}), new esri.symbol.SimpleLineSymbol(
				esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([ 193,
						131, 131 ]), 5));
		traceRT.gisAttr.lineLayer.add(traceRT.gisAttr.lineRoad);

		traceRT.gisAttr.lineWork = new esri.Graphic(new esri.geometry.Polyline(
				{
					"paths" : [ [ [] ] ]
				}), new esri.symbol.SimpleLineSymbol(
				esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([ 0,
						255, 0 ]), 5));
		traceRT.gisAttr.lineLayer.add(traceRT.gisAttr.lineWork);
		traceRT.gisAttr.pointCache = new jsYao.mapUtil.gisPointsCache();

		var deviceId = jsYao.getURLParam("deviceId");
		var beginTime = decodeURIComponent(jsYao.getURLParam("beginTime"));
		var endTime = decodeURIComponent(jsYao.getURLParam("endTime"));
		if (deviceId == null || deviceId == "" || beginTime == null
				|| beginTime == "" || endTime == null || endTime == "") {
			alert("查询参数不足!");
			return;
		}
		// new Date().Format("yyyy-MM-dd")
		traceRT.load(deviceId, beginTime, endTime);
		// 48677 2018-03-05 00:00:00 2018-03-05 23:59:59
	},
	load : function(deviceId, beginTime, endTime) {
		// 查轨迹
		$
				.ajax({
					type : 'get',
					async : false,
					url : "http://tapi.linkio.cn:8080/v1/trace/line",
					data : {
						"deviceId" : deviceId,
						"beginTime" : beginTime,
						"endTime" : endTime,
						"format" : "jsonp"
					},
					dataType : "jsonp",
					jsonp : 'callback',
					success : function(json, status, xhr) {
						if (xhr.status != "200" || json.length == 0) {
							alert("当日无轨迹，请启动农机后刷新页面。");
							$("#loading").hide();
							return;
						}

						if (json.code > 200) {
							alert("状态码：" + json.code + "，请联系客服!");
							return;
						}

						for ( var i = 0; i < json.length; i++) {
							traceRT.gisAttr.lineRoad.geometry.paths[i] = [];
							traceRT.gisAttr.lineWork.geometry.paths[i] = [];
							// isWork 0 非作业 1 作业
							if (json[i].isWork == "0") {

								for ( var j = 0; j < json[i].trace.length; j++) {
									var lng = json[i].trace[j].geometry.coordinates[0];
									var lat = json[i].trace[j].geometry.coordinates[1];

									traceRT.runStep(json[i].trace[j]);
									// 图像点
									// traceRT.imagePoint(json[i].trace[j]);
									traceRT.gisAttr.lineRoad.geometry.paths[i][j] = [
											lng, lat ];
								}
							} else {

								for ( var k = 0; k < json[i].trace.length; k++) {
									var lng = json[i].trace[k].geometry.coordinates[0];
									var lat = json[i].trace[k].geometry.coordinates[1];

									traceRT.runStep(json[i].trace[k]);
									// 画图像点
									// traceRT.imagePoint(json[i].trace[k]);
									traceRT.gisAttr.lineWork.geometry.paths[i][k] = [
											lng, lat ];
								}
							}

						}

						var p = new esri.geometry.Point(
								traceRT.runParam.lngLast,
								traceRT.runParam.latLast);
						traceRT.gisAttr.map.centerAndZoom(p, 13);
						var markerCar = new esri.Graphic(
								p,
								new esri.symbol.PictureMarkerSymbol(
										"http://hit.huidatech.cn/image/carTrace/icon_car_map_0.png",
										32, 32), {});
						markerCar.symbol
								.setAngle(Number(traceRT.runParam.course) - 90);
						traceRT.gisAttr.lineLayer.add(markerCar);
						$("#loading").hide();
					},
					error : function(json, status, xhr) {
						alert("当日无轨迹，请启动农机后刷新页面。");
						$("#loading").hide();
					}
				});
	},
	runStep : function(p) {
		var that = this;
		var info = p;

		var gpsTime = info.properties.gpsTime;// 定位时间
		var lng = info.geometry.coordinates[0];// 经度
		var lat = info.geometry.coordinates[1];// 纬度
		var machineId = info.properties.machineId; // 机具识别号
		var deep = info.properties.deep; // 深度

		if (lat > 1 && lng > 1) {
			// 定位点
			var dis = traceRT.runParam.latLast == null ? 10000000
					: jsYao.mapUtil.getDistance2(lat, lng,
							traceRT.runParam.latLast, traceRT.runParam.lngLast);
			var depthFlag = 0;
			var identifyFlag = 0;

			if (machineId != null && machineId != "") {
				var plug = machineId.split(",");

				for ( var i = 0; i < plug.length; i++) {
					// 出厂设置1357、2468 出场未设置 65535
					if (plug[i] != null && plug[i] != "" && plug[i] != "0"
							&& plug[i] != "00" && plug[i] != "1357"
							&& plug[i] != "2468" && plug[i] != "65535") {

						if (!traceRT.runParam.machines[plug[i]]) {
							traceRT.runParam.machines[plug[i]] = this
									.getMachine(plug[i]);
						}
						if (!traceRT.runParam.machines["nowaday"]
								|| traceRT.runParam.machines["nowaday"] == null) {
							traceRT.runParam.machines["nowaday"] = plug[i];
							identifyFlag = 1;
						} else if (traceRT.runParam.machines["nowaday"] == plug[i]) {
							identifyFlag = 1;
						}
					}
				}
			}

			var depth = Number(deep);
			if (depth > 0) {
				var aa = 0;
			}
			if (identifyFlag == 0)
				depthFlag = 0;
			else {
				var mac = traceRT.runParam.machines[traceRT.runParam.machines["nowaday"]];
				if (depth > traceRT.computeAttr.depthMax
						|| depth < traceRT.computeAttr.depthMin)
					depthFlag = 0;
				else if (mac["schemeDepth"] && depth < mac["schemeDepth"])
					depthFlag = 2;
				else
					depthFlag = 1;
			}

			if (depthFlag > 0) {
				// 测量点
				if (traceRT.runParam.depthFlagLast == 0) {
					// 上一个点是普通点
					if (depthFlag == 1)
						traceRT.runParam.surveyPointsQualified++;
					var s = depthFlag == 1 ? traceRT.gisAttr.pointSym1
							: traceRT.gisAttr.pointSym2;
					var p = traceRT.gisAttr.pointCache.getPoint(s);
					p.setAttributes({
						"gpsTime" : gpsTime,
						"lat" : lat,
						"lng" : lng,
						"depth" : depth,
						"workMode" : mac.workMode
					});
					p.geometry.setX(info.geometry.coordinates[0]);
					p.geometry.setY(info.geometry.coordinates[1]);

					traceRT.runParam.latLast = lat;
					traceRT.runParam.lngLast = lng;
				} else if (dis >= traceRT.computeAttr.pyDisMin) {
					// 上一个点是测量点并且大于最小漂移点
					if (depthFlag == 1)
						traceRT.runParam.surveyPointsQualified++;
					var s = depthFlag == 1 ? traceRT.gisAttr.pointSym1
							: traceRT.gisAttr.pointSym2;
					var p = traceRT.gisAttr.pointCache.getPoint(s);
					p.setAttributes({
						"gpsTime" : gpsTime,
						"lat" : lat,
						"lng" : lng,
						"depth" : depth,
						"workMode" : mac.workMode
					});
					p.geometry.setX(info.geometry.coordinates[0]);
					p.geometry.setY(info.geometry.coordinates[1]);

					traceRT.runParam.latLast = lat;
					traceRT.runParam.lngLast = lng;
				}
				traceRT.runParam.depthFlagLast = depthFlag;
			} else {
				traceRT.runParam.latLast = lat;
				traceRT.runParam.lngLast = lng;
				traceRT.runParam.depthFlagLast = depthFlag;
			}
		}
	},
	getMachine : function(identifyId) {
		return {
			machineId : 0,
			identifyId : identifyId,
			machineName : "未知农具",
			width : 3,
			workType : 0,
			workMode : 0,
			schemeDepth : 30
		};
	}

};
