$(document).ready(function() {
	$("#send").click(function() {
		var phone = $("#phone").val();
		send(phone);
	});

});
function send(phone) {
	$.ajax({
		type : 'POST',
		async : false,
		url : "http://localhost:9090/PrecisionWork/action/app",
//		url : "http://47.104.180.201:8999/PrecisionWork/action/weChat",
		data : {
			handler : "sendSMS",
			phone : phone
		},
		dataType : "jsonp",
		jsonp : 'jsonpcallback',
		success : function(json) {
			if (!json.success) {
				alert(json.msg);
				return;
			}
			
			$("#yzm").append(
					"<br><br>" + new Date() + "<br>" + json);
		},
		error : function(e) {
			alert('异常！');
		}
	});

}