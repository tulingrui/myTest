$(document).ready(function() {
	$("#send").click(function() {
		var phone = $("#phone").val();
		send(phone);
	});

});
function send(phone) {
	$.ajax({
		type : 'POST',
		async : false,
		url : "http://localhost:9090/PrecisionWork/action/sendSMS",
		data : {
			handler : "sendSMS_SUBHOOK",
			phone : phone
		},
		dataType : "jsonp",
		jsonp : 'jsonpcallback',
		success : function(json) {
			if (!json.success) {
				alert(json.msg);
				return;
			}
			$("#yzm").append(
					"<br><br>" + new Date() + "<br>" + json.jsonObject[0]);
		},
		error : function(e) {
			alert('异常！');
		}
	});

}