package gpcs.hd.support.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServiceContextFactory {

	private static ThreadLocal<ServiceContext> contexts = new ThreadLocal<ServiceContext>();

	public static void setContext() {
		ServiceContext bean = new ServiceContext();
		contexts.set(bean);
	}

	public static void setContext(HttpServletRequest request,
			HttpServletResponse response) {
		ServiceContext bean = new ServiceContext();
		bean.setRequest(request);
		bean.setResponse(response);
		contexts.set(bean);
	}

	/**
	 * 
	 * 获得当前线程的SESSIONBEAN
	 * 
	 * @return
	 */
	public static ServiceContext getContext() {
		ServiceContext bean = contexts.get();
		if (bean == null) {
			bean = new ServiceContext();
			contexts.set(bean);
		}
		return bean;
	}

	/**
	 * 关闭SESSIONBEAN
	 * 
	 */
	public static void close() {
		contexts.remove();
	}

}
