package gpcs.hd.support.service;

public abstract class BaseLocalService {

	public BaseLocalService() {

	}

	protected ServiceContext getContext() {
		return ServiceContextFactory.getContext();
	}

	protected void serviceError() {
		getContext().setSuccess(false);
	}

	protected void addServiceInfo(String info) {
		getContext().setInfo(
				getContext().getInfo() == null ? info : getContext().getInfo()
						+ " " + info);
	}

}
