package gpcs.hd.support.test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.springframework.context.support.FileSystemXmlApplicationContext;

public abstract class BaseTest {

	public BaseTest(String path) {
		String url;
		try {
			url = URLDecoder.decode(this.getClass().getClassLoader()
					.getResource("").getPath(), "UTF-8");
			context = new FileSystemXmlApplicationContext(url.substring(0, url
					.length() - 16)
					+ path);
		} catch (UnsupportedEncodingException e) {
			context = null;
		}
	}

	public BaseTest() {
		String url;
		try {
			url = URLDecoder.decode(this.getClass().getClassLoader()
					.getResource("").getPath(), "UTF-8");
			context = new FileSystemXmlApplicationContext(url.substring(0, url
					.length() - 16)
					+ "WEB-INF/spring/application.xml");
		} catch (UnsupportedEncodingException e) {
			context = null;
		}
	}

	protected FileSystemXmlApplicationContext context;

}
