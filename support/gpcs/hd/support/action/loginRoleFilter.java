package gpcs.hd.support.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class loginRoleFilter implements Filter {

	public loginRoleFilter() {
		this.map = new HashMap<String, String>();
		this.map.put("/action/user", "userLogin");
	}

	private Map<String, String> map;

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		// 判断例外
		String url = req.getRequestURI();
		if (map.containsKey(url) && map.get(url).contains("")) {
			String method = req.getParameter("handler");
			if (map.get(url).equals("*") || map.get(url).contains(method)) {
				chain.doFilter(request, response);
				return;
			}
		}

		// 判断登陆
		Object user = req.getSession().getAttribute("");
		if (user == null) {
			if (req.getHeader("x-requested-with") == null) {
				String u = req.getRequestURL().toString();
				resp.sendRedirect(u.substring(0, u.indexOf(url)));
			} else {
				resp.setContentType("text/html;charset=UTF-8");
				resp.getWriter().print("{success:false,info:\"无用户信息，请登陆！\"}");
			}
		} else
			chain.doFilter(request, response);
	}

	// public String getIp(HttpServletRequest req) {
	// if (req.getHeader("x-forwarded-for") == null)
	// return req.getRemoteAddr();
	// else
	// return req.getHeader("x-forwarded-for");
	// }

	public void init(FilterConfig arg0) throws ServletException {

	}

	public void destroy() {

	}

}
