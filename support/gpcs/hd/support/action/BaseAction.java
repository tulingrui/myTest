package gpcs.hd.support.action;

import gpcs.hd.support.service.ServiceContext;
import gpcs.hd.support.service.ServiceContextFactory;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class BaseAction implements Controller {

    private String method = "handler";

    protected static String RESPONSE_TYPE_HTML = "text/html;charset=UTF-8";

    protected static String RESPONSE_TYPE_JSON = "application/json;charset=UTF-8";

    protected static String RESPONSE_TYPE_DEFAULT = "text/html;charset=UTF-8";

    protected static String RESPONSE_TYPE_ERROR = "text/html;charset=UTF-8";

    protected static Logger logger = Logger.getLogger("action log");

    public final ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ServiceContextFactory.setContext(request, response);
        getContext().getRequest().setCharacterEncoding("UTF-8");
        String methodName = null;
        if (isAsync()) {// 异步AJAX请求
            String json;
            try {
                methodName = request.getParameter(method);
                json = (String) this.getClass().getDeclaredMethod(methodName)
                        .invoke(this);
                response.setContentType(getContext().getResponseType() == null ? RESPONSE_TYPE_DEFAULT
                        : getContext().getResponseType());
            } catch (Exception e) {
                e.printStackTrace();
                // StringWriter sw = new StringWriter();
                // e.printStackTrace(new PrintWriter(sw, true));
                // logger.error("class:" + this.getClass().getName() +
                // ",mehtod:"
                // + methodName + ",错误信息：" + sw.toString());
                logger.error("class:" + this.getClass().getName() + ",mehtod:"
                        + methodName + ",错误信息：" + e.getMessage());
                serviceError();
                addServiceInfo("操作异常！");
                json = getAjaxJson();
                response.setContentType(RESPONSE_TYPE_ERROR);
            } finally {
                ServiceContextFactory.close();
            }
            if (!response.isCommitted())
                response.getOutputStream().write(json.getBytes("UTF-8"));
        } else {// 同步请求
            try {
                methodName = request.getParameter(method);
                this.getClass().getDeclaredMethod(methodName).invoke(this);
            } catch (Exception e) {
                logger.error("class:" + this.getClass().getName() + ",mehtod:"
                        + methodName + ",错误信息：" + e.getMessage());
                response.getOutputStream().write("操作异常！".getBytes("UTF-8"));
            } finally {
                ServiceContextFactory.close();
            }
        }
        return null;
    }

    protected boolean isAsync() {
        return getContext().getRequest().getHeader("x-requested-with") != null;
    }

    protected Cookie createCookie(String key, String value, int maxAge) {
        Cookie c = new Cookie(key, value);
        c.setMaxAge(maxAge);
        c.setPath("/");
        return c;
    }

    protected Cookie[] getCookie(String key) {
        return getContext().getRequest().getCookies();
    }

    protected void addCookie(Cookie c) {
        getContext().getResponse().addCookie(c);
    }

    protected void addSessionAttribute(String key, Object value) {
        getContext().getRequest().getSession().setAttribute(key, value);
    }

    protected Object getSessionAttribute(String key) {
        return getContext().getRequest().getSession().getAttribute(key);
    }

    protected static String getParam(String name) {
        return getContext().getRequest().getParameter(name);
    }

    protected String getParamByApp(String name) {
        String ParamValue = "";
        try {
            ParamValue = new String(getContext().getRequest()
                    .getParameter(name).getBytes("ISO-8859-1"), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ParamValue;

    }

    protected static ServiceContext getContext() {
        return ServiceContextFactory.getContext();
    }

    protected String getAjaxJson() {
        return getAjaxJson(new String[] {});
    }

    protected String getAjaxJson(String json) {
        return getAjaxJson(new String[] { json });
    }

    protected String getAjaxJson(String[] jsons) {
        StringBuilder aj = new StringBuilder(100);
        aj.append("{success:");
        aj.append(getContext().isSuccess());
        aj.append(",info:");
        aj.append(getContext().getInfo() == null ? null : "\""
                + getContext().getInfo() + "\"");
        aj.append(",jsonObject:[");
        for (String json : jsons) {
            aj.append(json);
            aj.append(",");
        }
        aj.append("{}]}");
        return aj.toString();
    }

    protected void serviceError() {
        getContext().setSuccess(false);
    }

    protected boolean isSuccess() {
        return getContext().isSuccess();
    }

    protected void addServiceInfo(String info) {
        getContext().setInfo(
                getContext().getInfo() == null ? info : getContext().getInfo()
                        + " " + info);
    }

    protected void setResponseType(String type) {
        getContext().setResponseType(type);
    }

    protected void pageForward(String url, boolean isForward)
            throws IOException, ServletException {
        if (isForward)
            getContext()
                    .getRequest()
                    .getRequestDispatcher(url)
                    .forward(getContext().getRequest(),
                            getContext().getResponse());
        else
            getContext().getResponse().sendRedirect(url);
    }

}
