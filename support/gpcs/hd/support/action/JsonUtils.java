package gpcs.hd.support.action;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public final class JsonUtils {
	private static String dateType = "yyyy-MM-dd HH:mm:ss";
	private static List<String> simpleTypes;
	static {
		simpleTypes = new ArrayList<String>();
		simpleTypes.add("int");
		simpleTypes.add("Integer");
		simpleTypes.add("long");
		simpleTypes.add("Long");
		simpleTypes.add("double");
		simpleTypes.add("Double");
		simpleTypes.add("float");
		simpleTypes.add("Float");
		simpleTypes.add("boolean");
		simpleTypes.add("Boolean");
		simpleTypes.add("short");
		simpleTypes.add("Short");
		simpleTypes.add("char");
		simpleTypes.add("Character");
	}

	@SuppressWarnings("unchecked")
	public static String formatJson(Object obj)
			throws IllegalArgumentException, IllegalAccessException {
		if (obj == null)
			return "null";
		String classType = obj.getClass().getSimpleName();
		StringBuilder json = new StringBuilder(100);
		if (classType.equals("String"))
			return "\"" + obj + "\"";
		else if (classType.equals("Date"))
			return "\"" + new SimpleDateFormat(dateType).format(obj) + "\"";
		else if (simpleTypes.contains(classType))
			return obj.toString();
		else if (obj.getClass().isArray()) {
			json.append("[");
			if (Array.getLength(obj) > 0) {
				json.append(formatJson(Array.get(obj, 0)));
				for (int i = 1; i < Array.getLength(obj); i++) {
					json.append(",");
					json.append(formatJson(Array.get(obj, i)));
				}
			}
			json.append("]");
			return json.toString();
		} else if (obj instanceof Collection)
			return formatJson(((Collection) obj).toArray());
		else if (obj instanceof Map)
			return obj.toString();
		else {
			json.append("{");
			String type = null;
			Field[] fs = obj.getClass().getDeclaredFields();
			for (int i = 0; i < fs.length; i++) {
				if (i > 0)
					json.append(",");
				Field f = fs[i];
				f.setAccessible(true);
				json.append(f.getName());
				json.append(":");
				type = f.getType().getSimpleName();
				if ("String".equals(type) && f.get(obj) != null) {
					json.append("\"");
					json.append(f.get(obj));
					json.append("\"");
				} else if ("Date".equals(type) && f.get(obj) != null) {
					json.append("\"");
					json.append(new SimpleDateFormat(dateType).format(f
							.get(obj)));
					json.append("\"");
				} else if (simpleTypes.contains(type)) {
					json.append(f.get(obj));
				} else {
					json.append(formatJson(f.get(obj)));
				}
				f.get(obj);
				f.setAccessible(false);
			}
			json.append("}");
			return json.toString();
		}
	}
}
