package gpcs.hd.support.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class BaseDao extends JdbcDaoSupport {

    public BaseDao() {

    }

    protected List<String> sql_select(String sql, int resultCount) {
        return sql_select(sql, resultCount, "-");
    }

    public List<String> sql_select2(String sql, int resultCount,
            String resultSplit) {
        List<String> list = new ArrayList<String>();
        SqlRowSet rs = this.getJdbcTemplate().queryForRowSet(sql);
        StringBuffer sb;
        while (rs.next()) {
            sb = new StringBuffer();
            for (int i = 1; i <= resultCount; i++) {
                sb.append(rs.getObject(i) == null ? "null" : rs.getString(i));
                sb.append(resultSplit);
            }
            list.add(sb.toString());
        }
        return list;
    }

    protected List<String> sql_select(String sql, int resultCount,
            String resultSplit) {
        List<String> list = new ArrayList<String>();
        SqlRowSet rs = this.getJdbcTemplate().queryForRowSet(sql);
        StringBuffer sb;
        while (rs.next()) {
            sb = new StringBuffer();
            for (int i = 1; i <= resultCount; i++) {
                sb.append(rs.getObject(i) == null ? "null" : rs.getString(i));
                sb.append(resultSplit);
            }
            list.add(sb.toString());
        }
        return list;
    }

    protected List<String> sql_selectArray(String sql, int resultCount,
    		String resultSplit,String sqlstr) { 
    	List<String> list = new ArrayList<String>();
    	SqlRowSet rs = this.getJdbcTemplate().queryForRowSet(sql);
    	StringBuffer sb;
    	while (rs.next()) {
    		sb = new StringBuffer();
    		for (int i = 1; i <= resultCount; i++) {
    			sb.append(rs.getObject(i) == null ? "null" : rs.getString(i));
    			sb.append(resultSplit);
    		}
    		list.add(sb.toString());
    	}
    	//删除临时表
    	this.getJdbcTemplate().update(sqlstr);
    	return list;
    }
    
}
